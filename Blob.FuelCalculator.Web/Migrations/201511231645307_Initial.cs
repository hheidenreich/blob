using System.Data.Entity.Migrations;

namespace Blob.FuelCalculator.Web.Migrations
{
    public partial class Initial : DbMigration
    {
        #region Overrides

        public override void Down()
        {
            DropTable("dbo.DbVehicles");
            DropTable("dbo.DbUserAccounts");
            DropTable("dbo.DbRefuelings");
        }

        public override void Up()
        {
            CreateTable(
                "dbo.DbRefuelings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Amount = c.Double(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Distance = c.Double(nullable: false),
                        VehicleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DbUserAccounts",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.DbVehicles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Registration = c.String(nullable: false),
                        UserAccountName = c.String(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }

        #endregion
    }
}
