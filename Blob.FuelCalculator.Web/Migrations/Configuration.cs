using System;
using System.Data.Entity.Migrations;
using Blob.FuelCalculator.Web.Models;

namespace Blob.FuelCalculator.Web.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<FuelCalculatorDbContext>
    {
        #region Constructors

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        #endregion

        #region Protected Overrides

        protected override void Seed(Blob.FuelCalculator.Web.Models.FuelCalculatorDbContext context)
        {
            var guid1 = new Guid("28704F70-1928-4348-A754-736AC04E2DA5");
            var guid2 = new Guid("36026245-50FE-4106-89B5-73BC7E95A09A");
            var guid3 = new Guid("4F5A84AC-CE30-42E6-88F6-4959138C4790");
            var guid4 = new Guid("533031B3-1632-4946-B624-8A1795ED365A");

            context.UserAccounts.AddOrUpdate(u => u.Name,
                new DbUserAccount() { Name = "AB" },
                new DbUserAccount() { Name = "CD" }
            );

            context.Vehicles.AddOrUpdate(v => v.Id,
                new DbVehicle() { Id = guid1, Name = "Audi", Registration = "AB-AB 1", UserAccountName = "AB" },
                new DbVehicle() { Id = guid2, Name = "BMW", Registration = "AB-AB 2", UserAccountName = "AB" },
                new DbVehicle() { Id = guid3, Name = "Mercedes", Registration = "CD-CD 3", UserAccountName = "CD" },
                new DbVehicle() { Id = guid4, Name = "Porsche", Registration = "CD-CD 4", UserAccountName = "CD" }
            );

            context.Refuelings.AddOrUpdate(r => r.Id,
                new DbRefueling()
                { Amount = 20.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid1 },
                new DbRefueling()
                { Amount = 25.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid1 },
                new DbRefueling()
                { Amount = 30.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid2 },
                new DbRefueling()
                { Amount = 35.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid2 },
                new DbRefueling()
                { Amount = 40.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid3 },
                new DbRefueling()
                { Amount = 45.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid3 },
                new DbRefueling()
                { Amount = 50.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid4 },
                new DbRefueling()
                { Amount = 55.0, Date = DateTime.Now, Id = Guid.NewGuid(), Distance = 500.0, VehicleId = guid4 }
            );
        }

        #endregion
    }
}
