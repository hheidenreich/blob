﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Blob.FuelCalculator.Interfaces;
using Blob.FuelCalculator.Web.Models;

namespace Blob.FuelCalculator.Web.Controllers
{
    public class UserAccountsController : ApiController
    {
        #region Methods

        // GET: api/UserAccounts/ABC
        [ResponseType(typeof(IDbUserAccount))]
        public async Task<IHttpActionResult> GetUserAccount(string id)
        {
            var dbUserAccount = await _db.UserAccounts.FindAsync(id);
            if (dbUserAccount == null)
                return NotFound();
            return Ok(dbUserAccount);
        }

        // POST: api/UserAccounts
        public async Task<IHttpActionResult> PostUserAccount(DbUserAccount userAccount)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _db.UserAccounts.Add(userAccount);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserAccountExists(userAccount.Name))
                    return Conflict();
                throw;
            }

            return Ok();
        }

        #endregion

        #region Protected Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _db.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        #region Private Fields

        private readonly FuelCalculatorDbContext _db = new FuelCalculatorDbContext();

        #endregion

        #region Private Methods

        private bool UserAccountExists(string id) => _db.UserAccounts.Count(e => e.Name == id) > 0;

        #endregion
    }
}
