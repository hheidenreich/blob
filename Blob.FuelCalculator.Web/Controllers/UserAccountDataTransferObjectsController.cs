﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Blob.FuelCalculator.Interfaces;
using Blob.FuelCalculator.Web.Models;

namespace Blob.FuelCalculator.Web.Controllers
{
    public class UserAccountDataTransferObjectsController : ApiController
    {
        #region Methods

        // GET: api/UserAccountDataTransferObjects/ABC
        [ResponseType(typeof(UserAccountDataTransferObject))]
        public async Task<IHttpActionResult> GetUserAccountDataTransferObject(string id)
        {
            var userAccount = (await GetUserAccountDataTransferObjects()).FirstOrDefault(u => u.Name == id);
            if (userAccount == null)
                return NotFound();
            return Ok(userAccount);
        }

        // GET: api/UserAccountDataTransferObjects
        public async Task<IEnumerable<IUserAccountDataTransferObject>> GetUserAccountDataTransferObjects()
        {
            var userAccounts = await _db.UserAccounts.ToListAsync();
            var vehicles = await _db.Vehicles.ToListAsync();
            var refuelings = await _db.Refuelings.ToListAsync();

            var vehicleDataTransferObjects = vehicles.Select(v => new VehicleDataTransferObject()
            {
                Id = v.Id,
                Name = v.Name,
                Registration = v.Registration,
                UserAccountName = v.UserAccountName,
                Refuelings = refuelings.Where(r => r.VehicleId == v.Id).ToList<IDbRefueling>(),
            });

            var userAccountDataTransferObjects = userAccounts.Select(u => new UserAccountDataTransferObject()
            {
                Name = u.Name,
                Vehicles = vehicleDataTransferObjects
                    .Where(v => v.UserAccountName == u.Name)
                    .ToList<IVehicleDataTransferObject>()
            });

            return userAccountDataTransferObjects;
        }

        #endregion

        #region Protected Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _db.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        #region Private Fields

        private readonly FuelCalculatorDbContext _db = new FuelCalculatorDbContext();

        #endregion
    }
}
