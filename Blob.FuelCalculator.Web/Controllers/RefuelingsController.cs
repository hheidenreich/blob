﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Blob.FuelCalculator.Web.Models;

namespace Blob.FuelCalculator.Web.Controllers
{
    public class RefuelingsController : ApiController
    {
        #region Methods

        // DELETE: api/Refuelings/5
        public async Task<IHttpActionResult> DeleteRefueling(Guid id)
        {
            var refueling = await _db.Refuelings.FindAsync(id);
            if (refueling == null)
                return NotFound();

            _db.Refuelings.Remove(refueling);
            await _db.SaveChangesAsync();

            return Ok();
        }

        // POST: api/Refuelings
        public async Task<IHttpActionResult> PostRefueling(DbRefueling refueling)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _db.Refuelings.Add(refueling);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RefuelingExists(refueling.Id))
                    return Conflict();
                throw;
            }

            return Ok();
        }

        #endregion

        #region Protected Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _db.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        #region Private Fields

        private readonly FuelCalculatorDbContext _db = new FuelCalculatorDbContext();

        #endregion

        #region Private Methods

        private bool RefuelingExists(Guid id) => _db.Refuelings.Count(e => e.Id == id) > 0;

        #endregion
    }
}
