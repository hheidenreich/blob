﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Blob.FuelCalculator.Web.Models;

namespace Blob.FuelCalculator.Web.Controllers
{
    public class VehiclesController : ApiController
    {
        #region Methods

        // DELETE: api/Vehicles/5
        public async Task<IHttpActionResult> DeleteVehicle(Guid id)
        {
            var vehicle = await _db.Vehicles.FindAsync(id);
            if (vehicle == null)
                return NotFound();

            _db.Vehicles.Remove(vehicle);
            await _db.SaveChangesAsync();

            return Ok();
        }

        // POST: api/Vehicles
        public async Task<IHttpActionResult> PostVehicle(DbVehicle vehicle)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _db.Vehicles.Add(vehicle);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VehicleExists(vehicle.Id))
                    return Conflict();
                throw;
            }

            return Ok();
        }

        // PUT: api/Vehicles/5
        public async Task<IHttpActionResult> PutVehicle(Guid id, DbVehicle vehicle)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != vehicle.Id)
                return BadRequest();

            _db.Entry(vehicle).State = EntityState.Modified;

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VehicleExists(id))
                    return NotFound();
                return InternalServerError();
            }

            return Ok();
        }

        #endregion

        #region Protected Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _db.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        #region Private Fields

        private readonly FuelCalculatorDbContext _db = new FuelCalculatorDbContext();

        #endregion

        #region Private Methods

        private bool VehicleExists(Guid id) => _db.Vehicles.Count(e => e.Id == id) > 0;

        #endregion
    }
}
