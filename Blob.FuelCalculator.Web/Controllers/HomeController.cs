﻿using System.Web.Mvc;

namespace Blob.FuelCalculator.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Methods

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        #endregion
    }
}
