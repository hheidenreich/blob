﻿using System.Web.Mvc;

namespace Blob.FuelCalculator.Web
{
    public class FilterConfig
    {
        #region Static Methods

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        #endregion
    }
}
