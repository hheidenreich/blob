﻿using System.ComponentModel.DataAnnotations;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Web.Models
{
    public class DbUserAccount : IDbUserAccount
    {
        #region IDbUserAccount Properties

        [Key]
        public string Name { get; set; }

        #endregion
    }
}