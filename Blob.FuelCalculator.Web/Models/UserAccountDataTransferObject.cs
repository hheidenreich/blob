﻿using System.Collections.Generic;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Web.Models
{
    public class UserAccountDataTransferObject : DbUserAccount, IUserAccountDataTransferObject
    {
        #region IUserAccountDataTransferObject Properties

        public IList<IVehicleDataTransferObject> Vehicles { get; set; }

        #endregion
    }
}