using System;
using System.ComponentModel.DataAnnotations;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Web.Models
{
    public class DbVehicle : IDbVehicle
    {
        #region IDbVehicle Properties

        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^[A-Z]{1,3}[-][A-Z]{1,2} [0-9]{1,4}$")]
        public string Registration { get; set; }

        [Required]
        public virtual string UserAccountName { get; set; }

        #endregion
    }
}