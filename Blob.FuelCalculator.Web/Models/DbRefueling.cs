﻿using System;
using System.ComponentModel.DataAnnotations;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Web.Models
{
    public class DbRefueling : IDbRefueling
    {
        #region IDbRefueling Properties

        [Range(1.0, 200.0)]
        [Required]
        public double Amount { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Range(1.0, 2000.0)]
        [Required]
        public double Distance { get; set; }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public virtual Guid VehicleId { get; set; }

        #endregion
    }
}