﻿using System.Collections.Generic;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Web.Models
{
    public class VehicleDataTransferObject : DbVehicle, IVehicleDataTransferObject
    {
        #region IVehicleDataTransferObject Properties

        public IList<IDbRefueling> Refuelings { get; set; }

        #endregion
    }
}