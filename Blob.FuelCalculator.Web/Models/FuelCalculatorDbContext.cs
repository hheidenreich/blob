﻿using System.Data.Entity;

namespace Blob.FuelCalculator.Web.Models
{
    public class FuelCalculatorDbContext : DbContext
    {
        #region Constructors

        public FuelCalculatorDbContext()
            : base("name=FuelCalculatorDbContext")
        {
        }

        #endregion

        #region Properties

        public DbSet<DbRefueling> Refuelings { get; set; }
        public DbSet<DbUserAccount> UserAccounts { get; set; }
        public DbSet<DbVehicle> Vehicles { get; set; }

        #endregion
    }
}
