﻿using System;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IDbRefueling
    {
        #region Properties

        double Amount { get; }
        DateTime Date { get; }
        double Distance { get; }
        Guid Id { get; }
        Guid VehicleId { get; }

        #endregion
    }
}
