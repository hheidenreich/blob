﻿using System.Collections.Generic;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IUserAccountDataTransferObject : IDbUserAccount
    {
        #region Properties

        IList<IVehicleDataTransferObject> Vehicles { get; }

        #endregion
    }
}