﻿using System;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IRefueling : IDbRefueling, IComparable
    {
        #region Properties

        double Consumption { get; }

        #endregion
    }
}