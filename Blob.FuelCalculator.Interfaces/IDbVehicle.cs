﻿using System;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IDbVehicle
    {
        #region Properties

        Guid Id { get; }
        string Name { get; }
        string Registration { get; }
        string UserAccountName { get; }

        #endregion
    }
}
