﻿namespace Blob.FuelCalculator.Interfaces
{
    public interface IStats
    {
        #region Properties

        double Amount { get; }
        double Consumption { get; }
        double Distance { get; }

        #endregion
    }
}
