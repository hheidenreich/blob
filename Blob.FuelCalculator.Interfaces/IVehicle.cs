using System;
using System.Collections.ObjectModel;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IVehicle : IDbVehicle, IComparable
    {
        #region Properties

        string FormattedDisplay { get; }
        ReadOnlyObservableCollection<IRefueling> Refuelings { get; }
        IStats Stats { get; }

        #endregion

        #region Methods

        void AddRefueling(IRefueling refueling);
        void DeleteRefueling(IRefueling refueling);
        void Edit(string name, string registration);
        void SortRefuelings();

        #endregion
    }
}