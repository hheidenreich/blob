﻿using System.Collections.ObjectModel;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IUserAccount
    {
        #region Properties

        string Name { get; }
        ObservableCollection<IVehicle> Vehicles { get; }

        #endregion
    }
}
