﻿using System.Collections.Generic;

namespace Blob.FuelCalculator.Interfaces
{
    public interface IVehicleDataTransferObject : IDbVehicle
    {
        #region Properties

        IList<IDbRefueling> Refuelings { get; }

        #endregion
    }
}