﻿namespace Blob.FuelCalculator.Interfaces
{
    public interface IDbUserAccount
    {
        #region Properties

        string Name { get; }

        #endregion
    }
}