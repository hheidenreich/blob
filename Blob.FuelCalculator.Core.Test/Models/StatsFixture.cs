﻿using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Validation;
using NUnit.Framework;

namespace Blob.FuelCalculator.Core.Test.Models
{
    [TestFixture]
    public class StatsFixture
    {
        [Test]
        public void IsValid()
        {
            var result = StatsValidator.IsValid(100.0, 1000.0);

            Assert.IsTrue(result);
        }

        [TestCase(-1.0, 1000.0)]
        [TestCase(100.0, -1.0)]
        public void IsValid_InvalidAmountOrDistance(double amount, double distance)
        {
            var result = StatsValidator.IsValid(amount, distance);

            Assert.IsFalse(result);
        }

        [Test]
        public void NewStats()
        {
            var stats = new Stats(100.0, 1000.0);

            Assert.AreEqual(10.0, stats.Consumption);
        }

        [TestCase(-1.0, 1000.0)]
        [TestCase(100.0, -1.0)]
        public void NewStats_InvalidAmountOrDistance(double amount, double distance)
        {
            try
            {
                var stats = new Stats(amount, distance);

                Assert.Fail();
            }
            catch { /* ignored */ }
        }
    }
}
