﻿using System;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Validation;
using NUnit.Framework;

namespace Blob.FuelCalculator.Core.Test.Models
{
    [TestFixture]
    public class RefuelingFixture
    {
        [Test]
        public void Compare_Equal()
        {
            var refueling1 = new Refueling(Guid.Empty, DateTime.Parse("01.01.2000 0:0:0"), 100.0, 1000.0, Guid.Empty);
            var refueling2 = new Refueling(Guid.Empty, DateTime.Parse("01.01.2000 0:0:0"), 100.0, 1000.0, Guid.Empty);

            var result = refueling1.CompareTo(refueling2);

            Assert.AreEqual(result, 0);
        }

        [Test]
        public void Compare_Unequal()
        {
            var refueling1 = new Refueling(Guid.Empty, DateTime.Parse("01.01.1999 0:0:0"), 100.0, 1000.0, Guid.Empty);
            var refueling2 = new Refueling(Guid.Empty, DateTime.Parse("01.01.2000 0:0:0"), 100.0, 1000.0, Guid.Empty);

            var result = refueling1.CompareTo(refueling2);

            Assert.Less(result, 0);

            result = refueling2.CompareTo(refueling1);

            Assert.Greater(result, 0);
        }

        [Test]
        public void IsValid()
        {
            var result = RefuelingValidator.IsValid(100.0, 1000.0);

            Assert.IsTrue(result);
        }

        [TestCase(-1.0, 1000.0)]
        [TestCase(201.0, 1000.0)]
        [TestCase(100.0, -1.0)]
        [TestCase(100.0, 2001.0)]
        public void IsValid_InvalidAmountOrDistance(double amount, double distance)
        {
            var result = RefuelingValidator.IsValid(amount, distance);

            Assert.IsFalse(result);
        }

        [Test]
        public void NewRefueling()
        {
            try
            {
                var refueling = new Refueling(100.0, 1000.0, Guid.Empty);

                Assert.AreEqual(10.0, refueling.Consumption);
            }
            catch (Exception) { Assert.Fail(); }
        }

        [TestCase(-1.0, 1000.0)]
        [TestCase(201.0, 1000.0)]
        [TestCase(100.0, -1.0)]
        [TestCase(100.0, 2001.0)]
        public void NewRefueling_InvalidAmountOrDistance(double amount, double distance)
        {
            try
            {
                var refueling = new Refueling(amount, distance, Guid.Empty);

                Assert.Fail();
            }
            catch { /* ignored */ }
        }
    }
}