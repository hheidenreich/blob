﻿using System;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Validation;
using NUnit.Framework;

namespace Blob.FuelCalculator.Core.Test.Models
{
    [TestFixture]
    public class VehicleFixture
    {
        [TestCase(25.0, 500.0, 50.0, 500.0)]
        [TestCase(33.3, 500.0, 66.6, 500.0)]
        public void AddMultipleRefuelings(double amount1, double distance1, double amount2, double distance2)
        {
            var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

            vehicle.AddRefueling(new Refueling(amount1, distance1, Guid.Empty));

            Assert.AreEqual(1, vehicle.Refuelings.Count);
            Assert.AreEqual(amount1, vehicle.Refuelings[0].Amount);
            Assert.AreEqual(Math.Round(amount1 / distance1 * 100.0, 2, MidpointRounding.AwayFromZero),
                vehicle.Refuelings[0].Consumption);
            Assert.AreEqual(distance1, vehicle.Refuelings[0].Distance);
            Assert.AreEqual(amount1, vehicle.Stats.Amount);
            Assert.AreEqual(Math.Round(amount1 / distance1 * 100.0, 2, MidpointRounding.AwayFromZero),
                vehicle.Stats.Consumption);
            Assert.AreEqual(distance1, vehicle.Stats.Distance);

            vehicle.AddRefueling( // second refueling gets inserted before the first!
                new Refueling(amount2, distance2, Guid.Empty));

            Assert.AreEqual(2, vehicle.Refuelings.Count);
            Assert.AreEqual(amount2, vehicle.Refuelings[0].Amount);
            Assert.AreEqual(Math.Round(amount2 / distance2 * 100.0, 2, MidpointRounding.AwayFromZero),
                vehicle.Refuelings[0].Consumption);
            Assert.AreEqual(distance2, vehicle.Refuelings[0].Distance);
            Assert.AreEqual(amount1 + amount2, vehicle.Stats.Amount);
            Assert.AreEqual(
                Math.Round((amount1 + amount2) / (distance1 + distance2) * 100.0, 2, MidpointRounding.AwayFromZero),
                vehicle.Stats.Consumption);
            Assert.AreEqual(distance1 + distance2, vehicle.Stats.Distance);
        }

        [TestCase(25.0, 500.0)]
        [TestCase(33.3, 500.0)]
        [TestCase(50.0, 500.0)]
        public void AddRefueling(double amount, double distance)
        {
            var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

            vehicle.AddRefueling(new Refueling(amount, distance, Guid.Empty));

            Assert.AreEqual(1, vehicle.Refuelings.Count);
            Assert.AreEqual(amount, vehicle.Refuelings[0].Amount);
            Assert.AreEqual(Math.Round(amount / distance * 100.0, 2, MidpointRounding.AwayFromZero),
                vehicle.Refuelings[0].Consumption);
            Assert.AreEqual(distance, vehicle.Refuelings[0].Distance);
            Assert.AreEqual(amount, vehicle.Stats.Amount);
            Assert.AreEqual(Math.Round(amount / distance * 100.0, 2, MidpointRounding.AwayFromZero),
                vehicle.Stats.Consumption);
            Assert.AreEqual(distance, vehicle.Stats.Distance);
        }

        [Test]
        public void AddRefueling_InvalidRefueling()
        {
            try
            {
                var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

                vehicle.AddRefueling(null);

                Assert.Fail();
            }
            catch { /* ignored */ }
        }

        [Test]
        public void Compare_Equal()
        {
            var vehicle1 = new Vehicle("Vehicle 1", "A-B 1", null);
            var vehicle2 = new Vehicle("Vehicle 2", "A-B 1", null);

            var result = vehicle1.CompareTo(vehicle2);

            Assert.AreEqual(result, 0);
        }

        [Test]
        public void Compare_Unequal()
        {
            var vehicle1 = new Vehicle("Vehicle 1", "A-B 1", null);
            var vehicle2 = new Vehicle("Vehicle 2", "A-B 2", null);

            var result = vehicle1.CompareTo(vehicle2);

            Assert.Less(result, 0);

            result = vehicle2.CompareTo(vehicle1);

            Assert.Greater(result, 0);
        }

        [Test]
        public void DeleteRefueling()
        {
            var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);
            vehicle.AddRefueling(new Refueling(50.0, 1000.0, vehicle.Id));

            vehicle.DeleteRefueling(vehicle.Refuelings[0] as Refueling);

            Assert.AreEqual(0, vehicle.Refuelings.Count);
            Assert.AreEqual(0.0, vehicle.Stats.Amount);
            Assert.AreEqual(0.0, vehicle.Stats.Consumption);
            Assert.AreEqual(0.0, vehicle.Stats.Distance);
        }

        [Test]
        public void DeleteRefueling_InvalidRefueling()
        {
            try
            {
                var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

                vehicle.DeleteRefueling(null);

                Assert.Fail();
            }
            catch { /* ignored */ }

            try
            {
                var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

                vehicle.DeleteRefueling(new Refueling(100.0, 1000.0, Guid.Empty));

                Assert.Fail();
            }
            catch { /* ignored */ }
        }

        [Test]
        public void EditVehicle()
        {
            var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

            vehicle.Edit("Vehicle 2", "A-B 1");
            vehicle.Edit("Vehicle 2", "A-B 2");

            Assert.AreEqual("Vehicle 2", vehicle.Name);
            Assert.AreEqual("A-B 2", vehicle.Registration);
        }

        [TestCase(null, "A-B 1")]
        [TestCase("", "A-B 1")]
        [TestCase("Vehicle 1", null)]
        [TestCase("Vehicle 1", "")]
        [TestCase("Vehicle 1", "A B 1")]
        [TestCase("Vehicle 1", "A-B1")]
        [TestCase("Vehicle 1", "AB 1")]
        [TestCase("Vehicle 1", "AB1")]
        public void EditVehicle_InvalidNameOrRegistration(string newName, string newRegistration)
        {
            try
            {
                var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

                vehicle.Edit(newName, newRegistration);

                Assert.Fail();
            }
            catch { /* ignored */ }
        }

        [Test]
        public void IsValid()
        {
            var result = VehicleValidator.IsValid("Vehicle 1", "A-B 1");

            Assert.IsTrue(result);
        }

        [TestCase(null, "A-B 1")]
        [TestCase("", "A-B 1")]
        [TestCase("Vehicle 1", null)]
        [TestCase("Vehicle 1", "")]
        [TestCase("Vehicle 1", "A B 1")]
        [TestCase("Vehicle 1", "A-B1")]
        [TestCase("Vehicle 1", "AB 1")]
        [TestCase("Vehicle 1", "AB1")]
        public void IsValid_InvalidNameOrRegistration(string name, string registration)
        {
            var result = VehicleValidator.IsValid(name, registration);

            Assert.IsFalse(result);
        }

        [Test]
        public void NewVehicle()
        {
            var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);

            Assert.AreEqual("Vehicle 1", vehicle.Name);
            Assert.AreEqual("A-B 1", vehicle.Registration);
            Assert.AreEqual(0.0, vehicle.Stats.Amount);
            Assert.AreEqual(0.0, vehicle.Stats.Consumption);
            Assert.AreEqual(0.0, vehicle.Stats.Distance);
        }

        [TestCase(null, "A-B 1")]
        [TestCase("", "A-B 1")]
        [TestCase("Vehicle 1", null)]
        [TestCase("Vehicle 1", "")]
        [TestCase("Vehicle 1", "A B 1")]
        [TestCase("Vehicle 1", "A-B1")]
        [TestCase("Vehicle 1", "AB 1")]
        [TestCase("Vehicle 1", "AB1")]
        public void NewVehicle_InvalidNameOrRegistration(string name, string registration)
        {
            try
            {
                var vehicle = new Vehicle(name, registration, null);

                Assert.Fail();
            }
            catch { /* ignored */ }
        }

        [Test]
        public void SortRefuelings()
        {
            var vehicle = new Vehicle("Vehicle 1", "A-B 1", null);
            vehicle.AddRefueling(
                new Refueling(Guid.NewGuid(), DateTime.Parse("01.01.2000 0:0:0"), 10.0, 100.0, Guid.Empty));
            Assert.AreEqual(1, vehicle.Refuelings.Count);
            Assert.AreEqual(10.0, vehicle.Refuelings[0].Amount);
            Assert.AreEqual(100.0, vehicle.Refuelings[0].Distance);
            vehicle.AddRefueling( // second refueling gets inserted before the first!
                new Refueling(Guid.NewGuid(), DateTime.Parse("01.01.1999 0:0:0"), 20.0, 200.0, Guid.Empty));
            Assert.AreEqual(2, vehicle.Refuelings.Count);
            Assert.AreEqual(20.0, vehicle.Refuelings[0].Amount);
            Assert.AreEqual(200.0, vehicle.Refuelings[0].Distance);

            vehicle.SortRefuelings();

            Assert.AreEqual(2, vehicle.Refuelings.Count);
            Assert.AreEqual(10.0, vehicle.Refuelings[0].Amount);
            Assert.AreEqual(100.0, vehicle.Refuelings[0].Distance);
            Assert.AreEqual(20.0, vehicle.Refuelings[1].Amount);
            Assert.AreEqual(200.0, vehicle.Refuelings[1].Distance);
        }
    }
}
