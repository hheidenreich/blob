﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace Blob.FuelCalculator.Core.Test.Services
{
    [TestFixture]
    public class UserVehiclesServiceFixture
    {
        private readonly Guid _guid1 = new Guid("11111111-1111-1111-1111-111111111111");
        private readonly Guid _guid2 = new Guid("22222222-2222-2222-2222-222222222222");
        private readonly Guid _guid3 = new Guid("33333333-3333-3333-3333-333333333333");
        private readonly Guid _guid4 = new Guid("44444444-4444-4444-4444-444444444444");
        private readonly Guid _guid5 = new Guid("55555555-5555-5555-5555-555555555555");
        private readonly Guid _guid6 = new Guid("66666666-6666-6666-6666-666666666666");
        private readonly Guid _guid7 = new Guid("77777777-7777-7777-7777-777777777777");
        private readonly Guid _guid8 = new Guid("88888888-8888-8888-8888-888888888888");

        [Test]
        public async Task AddRefuelingAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            await userVehiclesService.AddRefuelingAsync(userVehiclesService.Vehicles[0] as Vehicle, 50.0, 200.0);

            Assert.AreEqual(5, userVehiclesService.Vehicles[0].Refuelings.Count);
            Assert.AreEqual(50.0, userVehiclesService.Vehicles[0].Refuelings[0].Amount);
            Assert.AreEqual(25.0, userVehiclesService.Vehicles[0].Refuelings[0].Consumption);
            Assert.AreEqual(200.0, userVehiclesService.Vehicles[0].Refuelings[0].Distance);
            Assert.AreEqual(150.0, userVehiclesService.Vehicles[0].Stats.Amount);
            Assert.AreEqual(15.0, userVehiclesService.Vehicles[0].Stats.Consumption);
            Assert.AreEqual(1000.0, userVehiclesService.Vehicles[0].Stats.Distance);
        }

        [TestCase(-1.0, 1000.0)]
        [TestCase(201.0, 1000.0)]
        [TestCase(100.0, -1.0)]
        [TestCase(100.0, 2001.0)]
        public async Task AddRefuelingAsync_InvalidAmountOrDistance(double amount, double distance)
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            try
            {
                await userVehiclesService.AddRefuelingAsync(
                    userVehiclesService.Vehicles[0] as Vehicle, amount, distance);
                Assert.Fail();
            }
            catch { /* ignored */ }
        }

        [Test]
        public async Task AddVehicleAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            await userVehiclesService.AddVehicleAsync("Vehicle 3", "A-B 3");

            Assert.AreEqual(3, userVehiclesService.Vehicles.Count);
            Assert.AreEqual("Vehicle 3", userVehiclesService.Vehicles[2].Name);
            Assert.AreEqual("A-B 3", userVehiclesService.Vehicles[2].Registration);
        }

        [TestCase("Vehicle 1", "A-B 1")]
        [TestCase("Vehicle 3", "A-B 1")]
        [TestCase(null, "A-B 3")]
        [TestCase("", "A-B 3")]
        [TestCase("Vehicle 3", null)]
        [TestCase("Vehicle 3", "")]
        [TestCase("Vehicle 3", "A B 3")]
        [TestCase("Vehicle 3", "A-B3")]
        [TestCase("Vehicle 3", "AB 3")]
        [TestCase("Vehicle 3", "AB3")]
        public async Task AddVehicleAsync_InvalidNameOrRegistration(string name, string registration)
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            try
            {
                await userVehiclesService.AddVehicleAsync(name, registration);
                Assert.Fail();
            }
            catch { /* ignored */ }
        }

        [Test]
        public async Task CanAddVehicleAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            var result = userVehiclesService.CanAddVehicle("Vehicle 3", "A-B 3");

            Assert.IsTrue(result);
        }

        [TestCase("Vehicle 1", "A-B 1")]
        [TestCase("Vehicle 3", "A-B 1")]
        [TestCase(null, "A-B 3")]
        [TestCase("", "A-B 3")]
        [TestCase("Vehicle 3", null)]
        [TestCase("Vehicle 3", "")]
        [TestCase("Vehicle 3", "A B 3")]
        [TestCase("Vehicle 3", "A-B3")]
        [TestCase("Vehicle 3", "AB 3")]
        [TestCase("Vehicle 3", "AB3")]
        public async Task CanAddVehicleAsync_InvalidNameOrRegistration(string name, string registration)
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            var result = userVehiclesService.CanAddVehicle(name, registration);

            Assert.IsFalse(result);
        }

        [Test]
        public async Task CanEditVehicleAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            var result = userVehiclesService.CanEditVehicle(
                userVehiclesService.Vehicles[0] as Vehicle, "Vehicle 3", "A-B 1");

            Assert.IsTrue(result);

            result = userVehiclesService.CanEditVehicle(
                userVehiclesService.Vehicles[0] as Vehicle, "Vehicle 3", "A-B 3");

            Assert.IsTrue(result);
        }

        [TestCase(null, "A-B 1")]
        [TestCase("", "A-B 1")]
        [TestCase("Vehicle 1", null)]
        [TestCase("Vehicle 1", "")]
        [TestCase("Vehicle 1", "A B 1")]
        [TestCase("Vehicle 1", "A-B1")]
        [TestCase("Vehicle 1", "AB 1")]
        [TestCase("Vehicle 1", "AB1")]
        public async Task CanEditVehicleAsync_InvalidNameOrRegistration(string newName, string newRegistration)
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");

            var result = userVehiclesService.CanEditVehicle(
                userVehiclesService.Vehicles[0] as Vehicle, newName, newRegistration);

            Assert.IsFalse(result);
        }

        [Test]
        public async Task DeleteRefuelingAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");
            Assert.Contains(vehicles[1], userVehiclesService.Vehicles);
            Assert.AreEqual(vehicles[1].Id, userVehiclesService.Vehicles[0].Id);
            Assert.Contains(vehicle1Refuelings[3], userVehiclesService.Vehicles[0].Refuelings);
            Assert.AreEqual(vehicle1Refuelings[3].Id, userVehiclesService.Vehicles[0].Refuelings[0].Id);

            await userVehiclesService.DeleteRefuelingAsync(
                userVehiclesService.Vehicles[0] as Vehicle, vehicle1Refuelings[3]);

            Assert.IsFalse(userVehiclesService.Vehicles[0].Refuelings.Contains(vehicle1Refuelings[3]));
        }

        [Test]
        public async Task DeleteVehicleAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");
            Assert.Contains(vehicles[1], userVehiclesService.Vehicles);
            Assert.AreEqual(vehicles[1].Id, userVehiclesService.Vehicles[0].Id);

            await userVehiclesService.DeleteVehicleAsync(userVehiclesService.Vehicles[0] as Vehicle);

            Assert.AreEqual(1, userVehiclesService.Vehicles.Count);
            Assert.AreEqual(vehicles[0].Id, userVehiclesService.Vehicles[0].Id);
        }

        [Test]
        public async Task EditVehicleAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);
            await userVehiclesService.StartAsync("DUMMY");
            Assert.Contains(vehicles[1], userVehiclesService.Vehicles);
            Assert.AreEqual(vehicles[1].Id, userVehiclesService.Vehicles[0].Id);

            await userVehiclesService.EditVehicleAsync(
                userVehiclesService.Vehicles[0] as Vehicle, "Vehicle 3", "A-B 1");

            Assert.AreEqual("Vehicle 3", userVehiclesService.Vehicles[0].Name);
            Assert.AreEqual("A-B 1", userVehiclesService.Vehicles[0].Registration);

            await userVehiclesService.EditVehicleAsync(
                userVehiclesService.Vehicles[0] as Vehicle, "Vehicle 3", "A-B 3");

            Assert.AreEqual("Vehicle 3", userVehiclesService.Vehicles[0].Name);
            Assert.AreEqual("A-B 3", userVehiclesService.Vehicles[0].Registration);
        }

        private IList<Refueling> GetVehicle1Refuelings()
        {
            // refuelings will be sorted by date in descending order in start async uf user vehicles svc
            var vehicle1Refuelings = new List<Refueling>()
            {
                new Refueling(_guid1, DateTime.Parse("01.01.2001 0:0:0"), 10.0, 200.0, _guid1),
                new Refueling(_guid2, DateTime.Parse("02.02.2002 0:0:0"), 20.0, 200.0, _guid1),
                new Refueling(_guid3, DateTime.Parse("03.03.2003 0:0:0"), 30.0, 200.0, _guid1),
                new Refueling(_guid4, DateTime.Parse("04.04.2004 0:0:0"), 40.0, 200.0, _guid1),
            };
            return vehicle1Refuelings;
        }

        private IList<Refueling> GetVehicle2Refuelings()
        {
            // refuelings will be sorted by date in descending order in start async uf user vehicles svc
            var vehicle2Refuelings = new List<Refueling>()
            {
                new Refueling(_guid5, DateTime.Parse("05.05.2005 0:0:0"), 10.0, 200.0, _guid2),
                new Refueling(_guid6, DateTime.Parse("06.06.2006 0:0:0"), 20.0, 200.0, _guid2),
                new Refueling(_guid7, DateTime.Parse("07.07.2007 0:0:0"), 30.0, 200.0, _guid2),
                new Refueling(_guid8, DateTime.Parse("08.08.2008 0:0:0"), 40.0, 200.0, _guid2),
            };
            return vehicle2Refuelings;
        }

        private List<Vehicle> GetVehicles(IList<Refueling> vehicle2Refuelings, IList<Refueling> vehicle1Refuelings)
        {
            // vehicles will be sorted by registration in ascending order in start async uf user vehicles svc
            var vehicles = new List<Vehicle>()
            {
                new Vehicle(_guid2, "Vehicle 2", "A-B 2", "DUMMY", vehicle2Refuelings),
                new Vehicle(_guid1, "Vehicle 1", "A-B 1", "DUMMY", vehicle1Refuelings),
            };
            return vehicles;
        }

        [Test]
        public async Task StartAsync()
        {
            var vehicle1Refuelings = GetVehicle1Refuelings();
            var vehicle2Refuelings = GetVehicle2Refuelings();
            var vehicles = GetVehicles(vehicle2Refuelings, vehicle1Refuelings);
            var userAcount = new UserAccount("DUMMY", vehicles);
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(true);
            mockWebStorageService.Setup(w => w.GetUserAccountAsync("DUMMY")).ReturnsAsync(userAcount);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);

            var result = await userVehiclesService.StartAsync("DUMMY");

            Assert.IsTrue(result);
            Assert.IsNotNull(userVehiclesService.Vehicles);
            Assert.AreEqual(2, userVehiclesService.Vehicles.Count);
            Assert.Contains(vehicles[0], userVehiclesService.Vehicles);
            Assert.Contains(vehicles[1], userVehiclesService.Vehicles);
            Assert.AreEqual(1, userVehiclesService.Vehicles.IndexOf(vehicles[0]));
            Assert.AreEqual(0, userVehiclesService.Vehicles.IndexOf(vehicles[1]));
            Assert.Contains(vehicle1Refuelings[0], userVehiclesService.Vehicles[0].Refuelings);
            Assert.Contains(vehicle1Refuelings[1], userVehiclesService.Vehicles[0].Refuelings);
            Assert.Contains(vehicle1Refuelings[2], userVehiclesService.Vehicles[0].Refuelings);
            Assert.Contains(vehicle1Refuelings[3], userVehiclesService.Vehicles[0].Refuelings);
            Assert.Contains(vehicle2Refuelings[0], userVehiclesService.Vehicles[1].Refuelings);
            Assert.Contains(vehicle2Refuelings[1], userVehiclesService.Vehicles[1].Refuelings);
            Assert.Contains(vehicle2Refuelings[2], userVehiclesService.Vehicles[1].Refuelings);
            Assert.Contains(vehicle2Refuelings[3], userVehiclesService.Vehicles[1].Refuelings);
            Assert.AreEqual(40.0, userVehiclesService.Vehicles[0].Refuelings[0].Amount);
            Assert.AreEqual(20.0, userVehiclesService.Vehicles[0].Refuelings[0].Consumption);
            Assert.AreEqual(200.0, userVehiclesService.Vehicles[0].Refuelings[0].Distance);
            Assert.AreEqual(100.0, userVehiclesService.Vehicles[0].Stats.Amount);
            Assert.AreEqual(12.5, userVehiclesService.Vehicles[0].Stats.Consumption);
            Assert.AreEqual(800.0, userVehiclesService.Vehicles[0].Stats.Distance);
        }

        [Test]
        public async Task StartAsync_NotAvailable()
        {
            var mockWebStorageService = new Mock<IWebStorageService>();
            mockWebStorageService.Setup(w => w.IsAvailableAsync()).ReturnsAsync(false);
            var userVehiclesService = new UserVehiclesService(mockWebStorageService.Object);

            var result = await userVehiclesService.StartAsync("DUMMY");

            Assert.IsFalse(result);
        }
    }
}
