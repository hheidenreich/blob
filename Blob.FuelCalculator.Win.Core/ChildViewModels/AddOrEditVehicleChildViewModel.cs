﻿using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Prism.Windows.AppModel;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels
{
    public abstract class AddOrEditVehicleChildViewModel : ChildViewModel, IAddOrEditVehicleChildViewModel
    {
        #region Constructors

        protected AddOrEditVehicleChildViewModel(IMessageService messageService, IResourceLoader resourceLoader)
            : base(messageService, resourceLoader)
        {
            Log.Info(null);
        }

        #endregion

        #region IAddOrEditVehicleChildViewModel Properties

        public bool IsValidVehicle
        {
            get { return _isValidVehicle; }
            protected set { SetProperty(ref _isValidVehicle, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); UpdateIsValidVehicle(); }
        }

        public string Registration
        {
            get { return _registration; }
            set { SetProperty(ref _registration, value); UpdateIsValidVehicle(); }
        }

        #endregion

        #region Protected Methods

        protected abstract void UpdateIsValidVehicle();

        #endregion

        #region Private Fields

        private bool _isValidVehicle;
        private string _name;
        private string _registration;

        #endregion
    }
}
