using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Prism.Commands;
using Prism.Windows.AppModel;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels
{
    public class AddVehicleChildViewModel : AddOrEditVehicleChildViewModel, IAddVehicleChildViewModel
    {
        #region Constructors

        public AddVehicleChildViewModel(IMessageService messageService,
                                        IResourceLoader resourceLoader,
                                        IUserVehiclesService userVehiclesService)
            : base(messageService, resourceLoader)
        {
            Log.Info(null);
            _userVehiclesService = userVehiclesService;
        }

        #endregion

        #region IAddVehicleChildViewModel Properties

        public ICommand AddVehicleCommand =>
            _addVehicleCommand ?? (_addVehicleCommand = DelegateCommand.FromAsyncHandler(AddVehicleAsync));

        #endregion

        #region Protected Overrides

        protected override void UpdateIsValidVehicle() =>
            IsValidVehicle = _userVehiclesService.CanAddVehicle(Name, Registration);

        #endregion

        #region Private Fields

        private readonly IUserVehiclesService _userVehiclesService;

        private ICommand _addVehicleCommand;

        #endregion

        #region Private Methods

        private async Task AddVehicleAsync()
        {
            try
            {
                Log.Verbose($"[{IsValidVehicle}|{Name}|{Registration}]");
                if (IsValidVehicle == false)
                    throw new InvalidOperationException("Invalid vehicle data.");
                await _userVehiclesService.AddVehicleAsync(Name, Registration);
            }   
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("AVCVM001", GetStringResource("AVCVM001"));
            }
        }

        #endregion
    }
}