﻿using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels
{
    public abstract class ChildViewModel : ViewModelBase, IChildViewModel
    {
        #region Constructors

        protected ChildViewModel(IMessageService messageService, IResourceLoader resourceLoader)
        {
            Log.Info(null);
            MessageService = messageService;
            ResourceLoader = resourceLoader;
        }

        #endregion

        #region IChildViewModel Properties

        public IMessageService MessageService { get; }

        #endregion

        #region IChildViewModel Methods

        public string GetStringResource(string uid) => ResourceLoader.GetString(uid);

        #endregion

        #region Protected Properties

        protected IResourceLoader ResourceLoader { get; }

        #endregion
    }
}