﻿using Blob.FuelCalculator.Core.Services.Interfaces;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces
{
    public interface IChildViewModel
    {
        #region Properties

        IMessageService MessageService { get; }

        #endregion

        #region Methods

        string GetStringResource(string uid);

        #endregion
    }
}