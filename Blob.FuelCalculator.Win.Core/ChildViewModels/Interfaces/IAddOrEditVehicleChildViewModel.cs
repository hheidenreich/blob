﻿namespace Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces
{
    public interface IAddOrEditVehicleChildViewModel : IChildViewModel
    {
        #region Properties

        bool IsValidVehicle { get; }
        string Name { get; set; }
        string Registration { get; set; }

        #endregion
    }
}