﻿using System.Windows.Input;
using Blob.FuelCalculator.Core.Models;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces
{
    public interface IEditVehicleChildViewModel : IAddOrEditVehicleChildViewModel
    {
        #region Properties

        ICommand EditVehicleCommand { get; }
        Vehicle Vehicle { get; }

        #endregion
    }
}
