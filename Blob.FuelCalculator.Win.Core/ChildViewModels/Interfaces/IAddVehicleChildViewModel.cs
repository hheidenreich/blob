﻿using System.Windows.Input;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces
{
    public interface IAddVehicleChildViewModel : IAddOrEditVehicleChildViewModel
    {
        #region Properties

        ICommand AddVehicleCommand { get; }

        #endregion
    }
}
