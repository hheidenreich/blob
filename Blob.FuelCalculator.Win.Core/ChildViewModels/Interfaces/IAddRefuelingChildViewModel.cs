using System.Windows.Input;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces
{
    public interface IAddRefuelingChildViewModel : IChildViewModel
    {
        #region Properties

        ICommand AddRefuelingCommand { get; }
        string AmountString { get; set; }
        string DistanceString { get; set; }
        bool IsValidRefueling { get; }

        #endregion
    }
}