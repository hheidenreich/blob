﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Prism.Commands;
using Prism.Windows.AppModel;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels
{
    public class AddRefuelingChildViewModel : ChildViewModel, IAddRefuelingChildViewModel
    {
        #region Constructors

        public AddRefuelingChildViewModel(IMessageService messageService,
                                          IResourceLoader resourceLoader,
                                          IUserVehiclesService userVehiclesService,
                                          Vehicle vehicle)
            : base(messageService, resourceLoader)
        {
            Log.Info(null);
            Log.Verbose($"{vehicle}");
            _userVehiclesService = userVehiclesService;
            _vehicle = vehicle;
        }

        #endregion

        #region IAddRefuelingChildViewModel Properties

        public ICommand AddRefuelingCommand =>
            _addRefuelingCommand ?? (_addRefuelingCommand = DelegateCommand.FromAsyncHandler(AddRefuelingAsync));

        public string AmountString
        {
            get { return _amountString; }
            set { SetProperty(ref _amountString, value); UpdateIsValidRefueling(); }
        }

        public string DistanceString
        {
            get { return _distanceString; }
            set { SetProperty(ref _distanceString, value); UpdateIsValidRefueling(); }
        }

        public bool IsValidRefueling
        {
            get { return _isValidRefueling; }
            private set { SetProperty(ref _isValidRefueling, value); }
        }

        #endregion

        #region Private Fields

        private readonly IUserVehiclesService _userVehiclesService;
        private readonly Vehicle _vehicle;

        private ICommand _addRefuelingCommand;
        private string _amountString;
        private string _distanceString;
        private bool _isValidRefueling;

        #endregion

        #region Private Methods

        private async Task AddRefuelingAsync()
        {
            try
            {
                Log.Verbose($"[{IsValidRefueling}|{AmountString}|{DistanceString}]");
                if (IsValidRefueling == false)
                    throw new InvalidOperationException("Invalid refueling data.");
                await _userVehiclesService.AddRefuelingAsync(
                    _vehicle, double.Parse(AmountString), double.Parse(DistanceString));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
            }
        }

        private void UpdateIsValidRefueling()
        {
            var amount = 0.0;
            var distance = 0.0;
            var cultureInfo = new CultureInfo("de");

            if (double.TryParse(AmountString, NumberStyles.Float, cultureInfo, out amount) &&
                double.TryParse(DistanceString, NumberStyles.Float, cultureInfo, out distance))
                IsValidRefueling = _userVehiclesService.IsValidRefueling(amount, distance);
            else
                IsValidRefueling = false;
        }

        #endregion
    }
}
