﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Prism.Commands;
using Prism.Windows.AppModel;

namespace Blob.FuelCalculator.Win.Core.ChildViewModels
{
    public class EditVehicleChildViewModel : AddOrEditVehicleChildViewModel, IEditVehicleChildViewModel
    {
        #region Constructors

        public EditVehicleChildViewModel(IMessageService messageService,
                                         IResourceLoader resourceLoader,
                                         IUserVehiclesService userVehiclesService,
                                         Vehicle vehicle)
            : base(messageService, resourceLoader)
        {
            _userVehiclesService = userVehiclesService;
            Log.Info(null);
            Log.Verbose($"{vehicle}");
            Vehicle = vehicle;
            Name = Vehicle.Name;
            Registration = Vehicle.Registration;
        }

        #endregion

        #region IEditVehicleChildViewModel Properties

        public ICommand EditVehicleCommand =>
            _editVehicleCommand ?? (_editVehicleCommand = DelegateCommand.FromAsyncHandler(EditVehicleAsync));

        public Vehicle Vehicle
        {
            get { return _vehicle; }
            private set { SetProperty(ref _vehicle, value); }
        }

        #endregion

        #region Protected Overrides

        protected override void UpdateIsValidVehicle() => 
            IsValidVehicle = _userVehiclesService.CanEditVehicle(Vehicle, Name, Registration);

        #endregion

        #region Private Fields

        private readonly IUserVehiclesService _userVehiclesService;

        private ICommand _editVehicleCommand;
        private Vehicle _vehicle;

        #endregion

        #region Private Methods

        private async Task EditVehicleAsync()
        {
            try
            {
                Log.Verbose($"[{IsValidVehicle}|{Name}|{Registration}] {Vehicle}");
                if (IsValidVehicle == false)
                    throw new InvalidOperationException("Invalid vehicle data.");
                await _userVehiclesService.EditVehicleAsync(Vehicle, Name, Registration);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("EVCVM001", GetStringResource("EVCVM001"));
            }
        }

        #endregion
    }
}
