﻿using System;
using System.Diagnostics.Tracing;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Blob.FuelCalculator.Core.Logging.Interfaces;

namespace Blob.FuelCalculator.Win.Core.Logging
{
    public class FileEventListener : EventListener, IFileEventListener
    {
        #region IFileEventListener Methods

        public async Task Start()
        {
            var localFolder = ApplicationData.Current.LocalFolder;

            try
            {
                var file = await localFolder.GetFileAsync("applog3.log");
                await file.DeleteAsync();

            }
            catch { /* ignored */ }

            try
            {
                var file = await localFolder.GetFileAsync("applog2.log");
                await file.RenameAsync("applog3.log");

            }
            catch { /* ignored */ }

            try
            {
                var file = await localFolder.GetFileAsync("applog1.log");
                await file.RenameAsync("applog2.log");

            }
            catch { /* ignored */ }

            _file = await ApplicationData.Current.LocalFolder.CreateFileAsync(
                "applog1.log", CreationCollisionOption.FailIfExists);
        }

        #endregion

        #region Protected Overrides

        protected override async void OnEventWritten(EventWrittenEventArgs eventData)
        {
            if (_file == null)
                return;

             await WriteToFile(
                 $"{DateTime.Now} {eventData.Level.ToString().ToUpper().Substring(0, 3)} {eventData.Payload[0]}");
        }

        #endregion

        #region Private Fields

        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);

        private StorageFile _file;

        #endregion

        #region Private Methods

        private async Task WriteToFile(string line)
        {
            await _semaphoreSlim.WaitAsync();

            try
            {
                await FileIO.AppendLinesAsync(_file, new[] { line });
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        #endregion
    }
}
