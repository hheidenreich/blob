﻿using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Microsoft.Practices.Unity;
using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;

namespace Blob.FuelCalculator.Win.Core.ViewModels
{
    public abstract class ViewModel : ViewModelBase, IViewModel
    {
        #region Constructors

        protected ViewModel(IMessageService messageService,
                            INavigationService navigationService,
                            IResourceLoader resourceLoader,
                            IUnityContainer unityContainer)
        {
            Log.Info(null);
            MessageService = messageService;
            NavigationService = navigationService;
            ResourceLoader = resourceLoader;
            UnityContainer = unityContainer;
        }

        #endregion

        #region IViewModel Properties

        public IMessageService MessageService { get; }

        #endregion

        #region IViewModel Methods

        public string GetStringResource(string uid) => ResourceLoader.GetString(uid);

        #endregion

        #region Protected Properties

        protected INavigationService NavigationService { get; }
        protected IResourceLoader ResourceLoader { get; }
        protected IUnityContainer UnityContainer { get; }

        #endregion
    }
}
