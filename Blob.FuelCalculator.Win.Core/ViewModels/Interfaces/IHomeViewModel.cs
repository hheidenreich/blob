using System.Windows.Input;

namespace Blob.FuelCalculator.Win.Core.ViewModels.Interfaces
{
    public interface IHomeViewModel : IViewModel
    {
        #region Properties

        bool IsValidUsername { get; }
        ICommand StartCommand { get; }
        string Username { get; set; }

        #endregion
    }
}