﻿using Blob.FuelCalculator.Core.Services.Interfaces;

namespace Blob.FuelCalculator.Win.Core.ViewModels.Interfaces
{
    public interface IViewModel
    {
        #region Properties

        IMessageService MessageService { get; }

        #endregion

        #region Methods

        string GetStringResource(string uid);

        #endregion
    }
}