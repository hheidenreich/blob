using System.Collections.ObjectModel;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;

namespace Blob.FuelCalculator.Win.Core.ViewModels.Interfaces
{
    public interface IRefuelingsViewModel : IViewModel
    {
        #region Properties

        ICommand DeleteRefuelingCommand { get; }

        IAddRefuelingChildViewModel GetAddRefuelingChildViewModel { get;  }
        bool IsRefuelingSelected { get; }
        ReadOnlyObservableCollection<IRefueling> Refuelings { get; }
        Refueling SelectedRefueling { get; set; }
        Vehicle Vehicle { get; }

        #endregion
    }
}