using System.Collections.ObjectModel;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;

namespace Blob.FuelCalculator.Win.Core.ViewModels.Interfaces
{
    public interface IVehiclesViewModel : IViewModel
    {
        #region Properties

        ICommand DeleteVehicleCommand { get; }
        IAddVehicleChildViewModel GetAddVehicleChildViewModel { get; }
        IEditVehicleChildViewModel GetEditVehicleChildViewModel { get; }
        bool IsBusy { get; }
        bool IsVehicleSelected { get; }
        ICommand NavigateToRefuelingsCommand { get; }
        Vehicle SelectedVehicle { get; set; }
        ReadOnlyObservableCollection<IVehicle> Vehicles { get; }

        #endregion
    }
}