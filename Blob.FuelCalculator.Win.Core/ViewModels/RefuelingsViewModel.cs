﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Windows.AppModel;
using Prism.Windows.Navigation;

namespace Blob.FuelCalculator.Win.Core.ViewModels
{
    public class RefuelingsViewModel : ViewModel, IRefuelingsViewModel
    {
        #region Constructors

        public RefuelingsViewModel(IMessageService errorInfoDisplayService,
                                   INavigationService navigationService,
                                   IResourceLoader resourceLoader,
                                   IUnityContainer unityContainer,
                                   IUserVehiclesService userVehiclesService)
            : base(errorInfoDisplayService, navigationService, resourceLoader, unityContainer)
        {
            Log.Info(null);
            _userVehiclesService = userVehiclesService;
        }

        #endregion

        #region IRefuelingsViewModel Properties

        public ICommand DeleteRefuelingCommand =>
            _deleteRefuelingCommand ?? (_deleteRefuelingCommand = DelegateCommand.FromAsyncHandler(DeleteRefuelingAsync));

        public IAddRefuelingChildViewModel GetAddRefuelingChildViewModel =>
            UnityContainer.Resolve<IAddRefuelingChildViewModel>(new ParameterOverride("vehicle", Vehicle));

        public bool IsRefuelingSelected
        {
            get { return _isRefuelingSelected; }
            private set { SetProperty(ref _isRefuelingSelected, value); }
        }

        public ReadOnlyObservableCollection<IRefueling> Refuelings { get; private set; }

        public Refueling SelectedRefueling
        {
            get { return _selectedRefueling; }
            set { SetProperty(ref _selectedRefueling, value); UpdateIsRefuelingSelected(); }
        }

        public Vehicle Vehicle
        {
            get { return _vehicle; }
            private set { SetProperty(ref _vehicle, value); }
        }

        #endregion

        #region Overrides

        public override async void OnNavigatedTo(NavigatedToEventArgs args, Dictionary<string, object> viewModelState)
        {
            try
            {
                Log.Info(null);
                if (args.Parameter == null)
                    throw new ArgumentNullException(nameof(args.Parameter));
                Vehicle = _userVehiclesService.Vehicles[(int)args.Parameter] as Vehicle;
                Refuelings = Vehicle?.Refuelings;
                base.OnNavigatedTo(args, viewModelState);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("RVM001", GetStringResource("RVM001"), true);
            }
        }

        #endregion

        #region Private Fields

        private readonly IUserVehiclesService _userVehiclesService;

        private ICommand _deleteRefuelingCommand;
        private bool _isRefuelingSelected;
        private Refueling _selectedRefueling;
        private Vehicle _vehicle;

        #endregion

        #region Private Methods

        private async Task DeleteRefuelingAsync()
        {
            try
            {
                Log.Verbose($"[{IsRefuelingSelected}] {SelectedRefueling}");
                if (!IsRefuelingSelected)
                    throw new InvalidOperationException("No refueling item selected.");
                _userVehiclesService.DeleteRefuelingAsync(Vehicle, SelectedRefueling);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("RVM002", GetStringResource("RVM002"), true);
            }
        }

        private void UpdateIsRefuelingSelected() => IsRefuelingSelected = SelectedRefueling != null;

        #endregion
    }
}
