﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Windows.AppModel;
using Prism.Windows.Navigation;

namespace Blob.FuelCalculator.Win.Core.ViewModels
{
    public class VehiclesViewModel : ViewModel, IVehiclesViewModel
    {
        #region Constructors

        public VehiclesViewModel(IMessageService messageService,
                                 INavigationService navigationService,
                                 IResourceLoader resourceLoader,
                                 IUnityContainer unityContainer,
                                 IUserVehiclesService userVehiclesService)
            : base(messageService, navigationService, resourceLoader, unityContainer)
        {
            Log.Info(null);
            _userVehiclesService = userVehiclesService;
        }

        #endregion

        #region IVehiclesViewModel Properties

        public ICommand DeleteVehicleCommand =>
            _deleteVehicleCommand ?? (_deleteVehicleCommand = DelegateCommand.FromAsyncHandler(DeleteVehicleAsync));

        public IAddVehicleChildViewModel GetAddVehicleChildViewModel => 
            UnityContainer.Resolve<IAddVehicleChildViewModel>();

        public IEditVehicleChildViewModel GetEditVehicleChildViewModel =>
            UnityContainer.Resolve<IEditVehicleChildViewModel>(new ParameterOverride("vehicle", SelectedVehicle));

        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        public bool IsVehicleSelected
        {
            get { return _isVehicleSelected; }
            private set { SetProperty(ref _isVehicleSelected, value); }
        }

        public ICommand NavigateToRefuelingsCommand =>
            _goToRefuelingsCommand ?? (_goToRefuelingsCommand = DelegateCommand.FromAsyncHandler(NavigateToRefuelingsAsync));

        public Vehicle SelectedVehicle
        {
            get { return _selectedVehicle; }
            set { if (SetProperty(ref _selectedVehicle, value)) UpdateIsVehicleSelected(); }
        }

        public ReadOnlyObservableCollection<IVehicle> Vehicles
        {
            get { return _vehicles; }
            set { SetProperty(ref _vehicles, value); }
        }

        #endregion

        #region Overrides

        public override async void OnNavigatedTo(NavigatedToEventArgs args, Dictionary<string, object> viewModelState)
        {
            try
            {
                Log.Info(null);
                if (args.Parameter == null)
                    throw new ArgumentException(nameof(args.Parameter));
                var username = args.Parameter as string;
                Log.Verbose($"[{username}]");
                IsBusy = true;
                if (!await _userVehiclesService.StartAsync(username))
                {
                    await MessageService.ShowMessageAsync("VVM001", GetStringResource("VVM001"));
                    NavigationService.GoBack();
                    return;
                }
                Vehicles = _userVehiclesService.Vehicles;
                base.OnNavigatedTo(args, viewModelState);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("VVM002", GetStringResource("VVM002"), true);
            }
            finally
            {
                IsBusy = false;
            }
        }

        #endregion

        #region Private Fields

        private readonly IUserVehiclesService _userVehiclesService;

        private ICommand _deleteVehicleCommand;
        private ICommand _goToRefuelingsCommand;
        private bool _isBusy;
        private bool _isVehicleSelected;
        private Vehicle _selectedVehicle;
        private ReadOnlyObservableCollection<IVehicle> _vehicles;

        #endregion

        #region Private Methods

        private async Task DeleteVehicleAsync()
        {
            try
            {
                Log.Verbose($"[{IsVehicleSelected}] {SelectedVehicle}");
                if (!IsVehicleSelected)
                    throw new InvalidOperationException("No vehicle item selected.");
                await _userVehiclesService.DeleteVehicleAsync(SelectedVehicle);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("VVM003", GetStringResource("VVM003"), true);
            }
        }

        private async Task NavigateToRefuelingsAsync()
        {
            try
            {
                Log.Verbose($"[{IsVehicleSelected}] {SelectedVehicle}");
                if (!IsVehicleSelected)
                    throw new InvalidOperationException("No vehicle item selected.");
                NavigationService.Navigate("Refuelings", Vehicles.IndexOf(SelectedVehicle));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("VVM004", GetStringResource("VVM004"), true);
            }
        }

        private void UpdateIsVehicleSelected() => IsVehicleSelected = SelectedVehicle != null;

        #endregion
    }
}
