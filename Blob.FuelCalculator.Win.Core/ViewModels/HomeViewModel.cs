﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.System;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Windows.AppModel;
using Prism.Windows.Navigation;

namespace Blob.FuelCalculator.Win.Core.ViewModels
{
    public class HomeViewModel : ViewModel, IHomeViewModel
    {
        #region Constructors

        public HomeViewModel(IMessageService errorInfoDisplayService,
                             INavigationService navigationService,
                             IResourceLoader resourceLoader,
                             IUnityContainer unityContainer)
            : base(errorInfoDisplayService, navigationService, resourceLoader, unityContainer)
        {
            Log.Info(null);
        }

        #endregion

        #region IHomeViewModel Properties

        public bool IsValidUsername
        {
            get { return _isValidUsername; }
            private set { SetProperty(ref _isValidUsername, value); }
        }

        public ICommand StartCommand =>
            _startCommand ?? (_startCommand = DelegateCommand.FromAsyncHandler(StartAsync));

        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); UpdateIsValidUsername(); }
        }

        #endregion

        #region Overrides

        public override async void OnNavigatedTo(NavigatedToEventArgs args, Dictionary<string, object> viewModelState)
        {
            try
            {
                Log.Info(null);
                base.OnNavigatedTo(args, viewModelState);
                await LoadSettings();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("HVM001", GetStringResource("HVM001"), true);
            }
        }

        public override async void OnNavigatingFrom(
            NavigatingFromEventArgs args, Dictionary<string, object> viewModelState, bool suspending)
        {
            try
            {
                Log.Info(null);
                base.OnNavigatingFrom(args, viewModelState, suspending);
                SaveSettings();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("HVM002", GetStringResource("HVM002"), true);
            }
        }

        #endregion

        #region Private Fields

        private bool _isValidUsername;
        private ICommand _startCommand;
        private string _username;

        #endregion

        #region Private Methods

        private async Task<string> GetUsername()
        {
            try
            {
                Log.Info(null);
                var users = await User.FindAllAsync();
                var displayName =
                    (await users[0].GetPropertyAsync(KnownUserProperties.DisplayName) as string)?.ToUpper();
                Log.Verbose($"[{displayName}]");
                return displayName;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("HVM003", GetStringResource("HVM003"), true);
                return null;
            }
        }

        private async Task LoadSettings()
        {
            try
            {
                Log.Info(null);
                var localSettings = ApplicationData.Current.LocalSettings;
                if (localSettings.Values.ContainsKey("Username") &&
                    !string.IsNullOrEmpty(localSettings.Values["Username"] as string))
                    Username = localSettings.Values["Username"] as string;
                else
                    Username = await GetUsername();
                Log.Verbose($"[{Username}]");
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        private void SaveSettings()
        {
            try
            {
                Log.Info(null);
                var localSettings = ApplicationData.Current.LocalSettings;
                localSettings.Values["Username"] = Username;
                Log.Verbose($"[{localSettings.Values["Username"]}]");
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        private async Task StartAsync()
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"[{IsValidUsername}|{Username}]");
                if (!IsValidUsername)
                    throw new InvalidOperationException("Invalid user name.");
                NavigationService.Navigate("Vehicles", Username);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await MessageService.ShowMessageAsync("HVM004", GetStringResource("HVM004"), true);
            }
        }

        private void UpdateIsValidUsername()
            => IsValidUsername = !string.IsNullOrEmpty(Username) && Username.Length >= 2;

        #endregion
    }
}
