﻿using System;
using System.Globalization;
using Windows.UI.Xaml.Data;
using Blob.FuelCalculator.Core.Logging;

namespace Blob.FuelCalculator.Win.Core.Converters
{
    public sealed class DateValueConverter : IValueConverter
    {
        #region IValueConverter Methods

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                if (!(value is DateTime))
                    return string.Empty;

                return ((DateTime)value).ToString("g", new CultureInfo("de"));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}