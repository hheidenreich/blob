using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Blob.FuelCalculator.Core.Logging;

namespace Blob.FuelCalculator.Win.Core.Converters
{
    public class InverseVisibilityConverter : IValueConverter
    {
        #region IValueConverter Methods

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                if (!(value is bool))
                    return Visibility.Visible;

                return (bool)value ? Visibility.Collapsed : Visibility.Visible;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}