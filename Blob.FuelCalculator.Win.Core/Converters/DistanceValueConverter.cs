using System;
using System.Globalization;
using Windows.UI.Xaml.Data;
using Blob.FuelCalculator.Core.Logging;

namespace Blob.FuelCalculator.Win.Core.Converters
{
    public sealed class DistanceValueConverter : IValueConverter
    {
        #region IValueConverter Methods

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                if (!(value is double))
                    return string.Empty;

                return ((double)value).ToString("F1", new CultureInfo("de")) + " km";
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}