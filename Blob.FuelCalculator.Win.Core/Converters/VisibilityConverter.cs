﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Blob.FuelCalculator.Core.Logging;

namespace Blob.FuelCalculator.Win.Core.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        #region IValueConverter Methods

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                if (!(value is bool))
                    return Visibility.Collapsed;

                return (bool)value ? Visibility.Visible : Visibility.Collapsed;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
