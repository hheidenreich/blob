﻿using System;
using System.Net.Http.Headers;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;

namespace Blob.FuelCalculator.Win.Core.Services
{
    public class WebStorageDataService : IWebStorageDataService
    {
        #region Constructors

        public WebStorageDataService(string clientBaseAddress,
                                     string mediaType,
                                     string refuelingsApi,
                                     string userAccountDataTransferObjectsApi,
                                     string userAccountsApi,
                                     string vehiclesApi)
        {
            Log.Verbose($"[{clientBaseAddress}|{mediaType}|{refuelingsApi}|{userAccountDataTransferObjectsApi}|" +
                        $"{userAccountsApi}|{vehiclesApi}]");
            ClientBaseAddress = new Uri(clientBaseAddress);
            JsonHeader = new MediaTypeWithQualityHeaderValue(mediaType);
            MediaType = mediaType;
            RefuelingsApi = refuelingsApi;
            UserAccountDataTransferObjectsApi = userAccountDataTransferObjectsApi;
            UserAccountsApi = userAccountsApi;
            VehiclesApi = vehiclesApi;
        }

        #endregion

        #region IWebStorageDataService Properties

        public Uri ClientBaseAddress { get; }
        public MediaTypeWithQualityHeaderValue JsonHeader { get; }
        public string MediaType { get; }
        public string RefuelingsApi { get; }
        public string UserAccountDataTransferObjectsApi { get; }
        public string UserAccountsApi { get; }
        public string VehiclesApi { get; }

        #endregion
    }
}
