﻿using System;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Blob.FuelCalculator.Core.Services.Interfaces;

namespace Blob.FuelCalculator.Win.Core.Services
{
    public class MessageService : IMessageService
    {
        #region IMessageService Methods

        public async Task ShowMessageAsync(string code, string message, bool shutdown = false)
        {
            var dlg = new MessageDialog(message, code);
            await dlg.ShowAsync();
            if (shutdown)
                Application.Current.Exit();
        }

        #endregion
    }
}
