#Blob.FuelCalculator

Bei diesem Projekt handelt es sich um eine �bungsarbeit im Rahmen der Ausbildung 
zum Microsoft Certified Solutions Developer (MCSD): Windows Store Apps.

Neben den Kursinhalten habe ich Wert darauf gelegt, aktuelle praxisorientierte
L�sungen wie Git, Prism, Unity, NUnit, Moq, ASP.NET MVC und die Universal
Windows Platform (UWP) zu verwenden.

Der jeweils aktuelle Stand befindet sich im Branch "Master", alle St�nde sind
mit Visual Studio 2015 kompilierbar und lauff�hig.

#Kontakt
info (at) blob (dot) de