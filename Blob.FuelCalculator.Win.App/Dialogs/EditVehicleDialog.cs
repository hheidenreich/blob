﻿using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;

namespace Blob.FuelCalculator.Win.App.Dialogs
{
    public sealed partial class EditVehicleDialog : ContentDialog
    {
        #region Constructors

        public EditVehicleDialog(IEditVehicleChildViewModel viewModel)
        {
            Log.Info(null);
            InitializeComponent();
            DataContext = viewModel;
            _viewModel = viewModel;
        }

        #endregion

        #region Private Fields

        private readonly IEditVehicleChildViewModel _viewModel;

        #endregion

        #region Private Methods

        private void OnTextBoxKeyUp(object sender, KeyRoutedEventArgs args)
        {
            if (IsPrimaryButtonEnabled && args.Key == VirtualKey.Enter)
            {
                PrimaryButtonCommand.Execute(null);
                Hide();
            }
        }

        private void OnVehicleNameTextBoxTextChanged(object sender, TextChangedEventArgs args)
        {
            _viewModel.Name = VehicleNameTextBox.Text;
        }

        private void OnVehicleRegistrationTextBoxTextChanged(object sender, TextChangedEventArgs args)
        {
            var selectionStart = VehicleRegistrationTextBox.SelectionStart;
            VehicleRegistrationTextBox.Text = VehicleRegistrationTextBox.Text.ToUpper();
            VehicleRegistrationTextBox.SelectionStart = selectionStart;
            _viewModel.Registration = VehicleRegistrationTextBox.Text;
        }

        #endregion
    }
}
