﻿using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;

namespace Blob.FuelCalculator.Win.App.Dialogs
{
    public sealed partial class AddRefuelingDialog : ContentDialog
    {
        #region Constructors

        public AddRefuelingDialog(IAddRefuelingChildViewModel viewModel)
        {
            Log.Info(null);
            InitializeComponent();
            DataContext = viewModel;
            _viewModel = viewModel;
        }

        #endregion

        #region Private Fields

        private readonly IAddRefuelingChildViewModel _viewModel;

        #endregion

        #region Private Methods

        private void OnRefuelingAmountTextBoxTextChanged(object sender, TextChangedEventArgs args)
        {
            _viewModel.AmountString = RefuelingAmountTextBox.Text;
        }

        private void OnRefuelingDistanceTextBoxTextChanged(object sender, TextChangedEventArgs args)
        {
            _viewModel.DistanceString = RefuelingDistanceTextBox.Text;
        }

        private void OnTextBoxKeyUp(object sender, KeyRoutedEventArgs args)
        {
            if (IsPrimaryButtonEnabled && args.Key == VirtualKey.Enter)
            {
                PrimaryButtonCommand.Execute(null);
                Hide();
            }
        }

        #endregion
    }
}
