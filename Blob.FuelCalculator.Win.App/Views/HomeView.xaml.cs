﻿using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Prism.Windows.Mvvm;

namespace Blob.FuelCalculator.Win.App.Views
{
    public sealed partial class HomeView : SessionStateAwarePage
    {
        #region Constructors

        public HomeView()
        {
            Log.Info(null);
            InitializeComponent();
            _viewModel = (IHomeViewModel)DataContext;
        }

        #endregion

        #region Private Fields

        private readonly IHomeViewModel _viewModel;

        #endregion

        #region Private Methods

        private void OnUsernameTextBoxKeyUp(object sender, KeyRoutedEventArgs args)
        {
            if (StartButton.IsEnabled && args.Key == VirtualKey.Enter)
                _viewModel.StartCommand.Execute(null);
        }

        private void OnUsernameTextBoxTextChanged(object sender, TextChangedEventArgs args)
        {
            var selectionStart = UsernameTextBox.SelectionStart;
            UsernameTextBox.Text = UsernameTextBox.Text.ToUpper();
            UsernameTextBox.SelectionStart = selectionStart;
            _viewModel.Username = UsernameTextBox.Text;
        }

        #endregion
    }
}
