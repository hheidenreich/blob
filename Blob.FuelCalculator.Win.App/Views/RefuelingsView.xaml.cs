﻿using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Prism.Windows.Mvvm;

namespace Blob.FuelCalculator.Win.App.Views
{
    public sealed partial class RefuelingsView : SessionStateAwarePage
    {
        #region Constructors

        public RefuelingsView()
        {
            Log.Info(null);
            InitializeComponent();
            _viewModel = DataContext as IRefuelingsViewModel;
        }

        #endregion

        #region Private Fields

        private readonly IRefuelingsViewModel _viewModel;

        #endregion

        #region Private Methods

        private async void OnAddRefuelingButtonClicked(object sender, RoutedEventArgs args)
        {
            try
            {
                Log.Info(null);
                var dlg = new Dialogs.AddRefuelingDialog(_viewModel.GetAddRefuelingChildViewModel);
                await dlg.ShowAsync();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await _viewModel.MessageService.ShowMessageAsync("UE001", _viewModel.GetStringResource("UE001"));
            }
        }

        private async void OnDeleteRefuelingButtonClicked(object sender, RoutedEventArgs args)
        {
            try
            {
                Log.Info(null);
                var dlg = new MessageDialog(_viewModel.GetStringResource("AreUSureUWantToDeleteTheRefueling"),
                                            _viewModel.GetStringResource("DeleteRefueling"));
                dlg.Commands.Add(new UICommand(_viewModel.GetStringResource("Yes"),
                                               x => _viewModel.DeleteRefuelingCommand.Execute(null)));
                dlg.Commands.Add(new UICommand(_viewModel.GetStringResource("No"), null));
                dlg.CancelCommandIndex = 1;
                dlg.DefaultCommandIndex = 1;
                await dlg.ShowAsync();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await _viewModel.MessageService.ShowMessageAsync("UE001", _viewModel.GetStringResource("UE001"));
            }
        }

        #endregion
    }
}
