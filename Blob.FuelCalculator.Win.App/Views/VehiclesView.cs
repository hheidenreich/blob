﻿using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Prism.Windows.Mvvm;

namespace Blob.FuelCalculator.Win.App.Views
{
    public sealed partial class VehiclesView : SessionStateAwarePage
    {
        #region Constructors

        public VehiclesView()
        {
            Log.Info(null);
            InitializeComponent();
            _viewModel = (IVehiclesViewModel)DataContext;
        }

        #endregion

        #region Private Fields

        private readonly IVehiclesViewModel _viewModel;

        #endregion

        #region Private Methods

        private async void OnAddVehicleButtonClicked(object sender, RoutedEventArgs args)
        {
            try
            {
                Log.Info(null);
                var dlg = new Dialogs.AddVehicleDialog(_viewModel.GetAddVehicleChildViewModel);
                await dlg.ShowAsync();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await _viewModel.MessageService.ShowMessageAsync("UE001", _viewModel.GetStringResource("UE001"));
            }
        }

        private async void OnDeleteVehicleButtonClicked(object sender, RoutedEventArgs args)
        {
            try
            {
                Log.Info(null);
                var dlg = new MessageDialog(_viewModel.GetStringResource("AreUSureUWantToDeleteTheVehicle"),
                                            _viewModel.GetStringResource("DeleteVehicle"));
                dlg.Commands.Add(new UICommand(_viewModel.GetStringResource("Yes"),
                                               x => _viewModel.DeleteVehicleCommand.Execute(null)));
                dlg.Commands.Add(new UICommand(_viewModel.GetStringResource("No"), null));
                dlg.CancelCommandIndex = 1;
                dlg.DefaultCommandIndex = 1;
                await dlg.ShowAsync();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await _viewModel.MessageService.ShowMessageAsync("UE001", _viewModel.GetStringResource("UE001"));
            }
        }

        private async void OnEditVehicleButtonClicked(object sender, RoutedEventArgs args)
        {
            try
            {
                Log.Info(null);
                var dlg = new Dialogs.EditVehicleDialog(_viewModel.GetEditVehicleChildViewModel);
                await dlg.ShowAsync();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                await _viewModel.MessageService.ShowMessageAsync("UE001", _viewModel.GetStringResource("UE001"));
            }
        }

        #endregion
    }
}
