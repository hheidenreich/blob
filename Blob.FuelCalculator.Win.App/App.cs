﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Win.Core.ChildViewModels;
using Blob.FuelCalculator.Win.Core.ChildViewModels.Interfaces;
using Blob.FuelCalculator.Win.Core.Logging;
using Blob.FuelCalculator.Win.Core.Services;
using Blob.FuelCalculator.Win.Core.ViewModels;
using Blob.FuelCalculator.Win.Core.ViewModels.Interfaces;
using Microsoft.Practices.Unity;
using Prism.Mvvm;
using Prism.Unity.Windows;
using Prism.Windows.AppModel;

namespace Blob.FuelCalculator.Win.App
{
    sealed partial class Application : PrismUnityApplication
    {
        #region Constructors

        public Application()
        {
            InitializeComponent();
            UnhandledException += OnUnhandledException;
        }

        #endregion

        #region Protected Overrides

        protected override Type GetPageType(string pageToken)
        {
            Log.Info($"[{pageToken}]");
            var viewAssemblyName = GetType().GetTypeInfo().AssemblyQualifiedName;
            var viewName = viewAssemblyName.Replace(GetType().FullName, GetType().Namespace + $".Views.{pageToken}View");
            var viewType = Type.GetType(viewName);
            Log.Info($"[{viewType}]");
            return viewType;
        }

        protected override async Task OnInitializeAsync(IActivatedEventArgs args)
        {
            await Log.Start(new DebugEventListener(), new FileEventListener());
            Log.Info($"{typeof(Application).GetTypeInfo().Assembly.FullName}");
            Log.Info($"{typeof(Log).GetTypeInfo().Assembly.FullName}");

            _webStorageDataService = new WebStorageDataService("http://localhost:52513/",
                                                               "application/json",
                                                               "api/Refuelings",
                                                               "api/UserAccountDataTransferObjects",
                                                               "api/UserAccounts",
                                                               "api/Vehicles");

            Container
                .RegisterInstance(NavigationService)
                .RegisterInstance(SessionStateService)
                .RegisterInstance<IResourceLoader>(new ResourceLoaderAdapter(new ResourceLoader()))
                .RegisterInstance<IWebStorageDataService>(_webStorageDataService);

            Container
                .RegisterType<IHttpClientService, HttpClientService>(new ContainerControlledLifetimeManager())
                .RegisterType<IJsonService, JsonService>(new ContainerControlledLifetimeManager())
                .RegisterType<IMessageService, MessageService>(new ContainerControlledLifetimeManager())
                .RegisterType<IUserVehiclesService, UserVehiclesService>(new ContainerControlledLifetimeManager())
                .RegisterType<IWebStorageService, WebStorageService>(new ContainerControlledLifetimeManager());

            Container
                .RegisterType<IHomeViewModel, HomeViewModel>(new ContainerControlledLifetimeManager())
                .RegisterType<IRefuelingsViewModel, RefuelingsViewModel>(new ContainerControlledLifetimeManager())
                .RegisterType<IVehiclesViewModel, VehiclesViewModel>(new ContainerControlledLifetimeManager());

            Container
                .RegisterType<IAddRefuelingChildViewModel, AddRefuelingChildViewModel>()
                .RegisterType<IAddVehicleChildViewModel, AddVehicleChildViewModel>()
                .RegisterType<IEditVehicleChildViewModel, EditVehicleChildViewModel>();

            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver(ViewTypeToViewModelTypeResolver);

            await base.OnInitializeAsync(args);
        }

        protected override Task OnLaunchApplicationAsync(LaunchActivatedEventArgs args)
        {
            Log.Info(null);
            NavigationService.Navigate("Home", null);
            Window.Current.Activate();
            return Task.FromResult<object>(null);
        }

        protected override Task OnSuspendingApplicationAsync()
        {
            Log.Info(null);
            Log.Stop();
            return base.OnSuspendingApplicationAsync();
        }

        #endregion

        #region Private Fields

        private WebStorageDataService _webStorageDataService;

        #endregion

        #region Private Methods

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Log.Info(null);
            // TODO: Überprüfen, ob und wann und weshalb der Handler aufgerufen wird
            Log.Error($"{GetType().Name}.{nameof(OnUnhandledException)} [{args.Exception.Message}|{args.Exception.StackTrace}]");
        }

        private Type ViewTypeToViewModelTypeResolver(Type viewType)
        {
            Log.Info($"[{viewType}]");
            var viewName = viewType.FullName.Replace("App.Views", "Core.ViewModels");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName.Replace("Win.App", "Win.Core");
            var viewModelName = $"{viewName.Substring(0, viewName.Length - 4)}ViewModel, {viewAssemblyName}";
            var viewModelType = Type.GetType(viewModelName);
            Log.Info($"[{viewModelType}]");
            return viewModelType;
        }

        #endregion
    }
}
