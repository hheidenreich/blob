﻿using System;
using Blob.FuelCalculator.Core.Logging;

namespace Blob.FuelCalculator.Core.Calculators
{
    public static class StatsCalculator
    {
        #region Static Methods

        public static double CalculateConsumption(double amount, double distance)
        {
            try
            {
                var result =
                    distance > 0.0 ? Math.Round(amount / distance * 100.0, 2, MidpointRounding.AwayFromZero) : 0.0;
                Log.Verbose($"[{amount}|{distance}|{result}]");
                return result;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion
    }
}
