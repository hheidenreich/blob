﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Core.Calculators
{
    public static class VehicleCalculator
    {
        #region Static Methods

        public static Stats CalculateVehicleStats(IList<IRefueling> refuelings)
        {
            try
            {
                var result = new Stats(refuelings.Sum(x => x.Amount), refuelings.Sum(x => x.Distance));
                Log.Verbose($"[{result}]");
                return result;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion
    }
}
