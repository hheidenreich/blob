﻿using System;
using Blob.FuelCalculator.Core.Calculators;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Validation;
using Blob.FuelCalculator.Interfaces;
using Newtonsoft.Json;
using Prism.Mvvm;

namespace Blob.FuelCalculator.Core.Models
{
    public class Refueling : BindableBase, IRefueling
    {
        #region Constructors

        public Refueling(double amount, double distance, Guid vehicleId)
            : this(Guid.NewGuid(), DateTime.Now, amount, distance, vehicleId)
        {
        }

        [JsonConstructor]
        public Refueling(Guid id, DateTime date, double amount, double distance, Guid vehicleId)
        {
            try
            {
                Log.Verbose($"[{id}|{date.ToString("d")}|{amount}|{distance}|{vehicleId}]");
                if (!RefuelingValidator.IsValid(amount, distance))
                    throw new ArgumentException("Invalid refueling data");
                Id = id;
                Date = date;
                Amount = amount;
                Distance = distance;
                Consumption = RefuelingCalculator.CalculateConsumption(amount, distance);
                VehicleId = vehicleId;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region IRefueling Properties

        public double Amount
        {
            get { return _amount; }
            private set { SetProperty(ref _amount, value); }
        }

        [JsonIgnore]
        public double Consumption
        {
            get { return _consumption; }
            private set { SetProperty(ref _consumption, value); }
        }

        public DateTime Date { get; }

        public double Distance
        {
            get { return _distance; }
            private set { SetProperty(ref _distance, value); }
        }

        public Guid Id { get; }
        public Guid VehicleId { get; }

        #endregion

        #region IRefueling Methods

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;

            var otherRefueling = obj as Refueling;
            if (otherRefueling == null)
                throw new ArgumentException("Object is not a refueling");

            return DateTime.Compare(Date, otherRefueling.Date);
        }

        #endregion

        #region Overrides

        public override string ToString() =>
            $"[{Id}|{Date.ToString("g")}|{Amount}|{Distance}|{Consumption}|{VehicleId}]";

        #endregion

        #region Private Fields

        private double _amount;
        private double _consumption;
        private double _distance;

        #endregion
    }
}
