﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using Blob.FuelCalculator.Core.Calculators;
using Blob.FuelCalculator.Core.Extensions;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Validation;
using Blob.FuelCalculator.Interfaces;
using Newtonsoft.Json;
using Prism.Mvvm;

namespace Blob.FuelCalculator.Core.Models
{
    public class Vehicle : BindableBase, IVehicle
    {
        #region Constructors

        [JsonConstructor]
        public Vehicle(Guid id,
                       string name,
                       string registration,
                       string userAccountName,
                       IList<Refueling> refuelings)
        {
            try
            {
                Log.Verbose($"[{id}|{name}|{registration}|{userAccountName}|{refuelings.Count}]");
                if (!VehicleValidator.IsValid(name, registration))
                    throw new ArgumentException("Invalid vehicle data");
                Id = id;
                Name = name;
                Registration = registration;
                UserAccountName = userAccountName;
                _refuelings = new ObservableCollection<IRefueling>(refuelings);
                Refuelings = new ReadOnlyObservableCollection<IRefueling>(_refuelings);
                Stats = VehicleCalculator.CalculateVehicleStats(Refuelings);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public Vehicle(string name, string registration, string userAccountName)
            : this(Guid.NewGuid(), name, registration, userAccountName, new List<Refueling>())
        {
        }

        #endregion

        #region IVehicle Properties

        [JsonIgnore]
        public string FormattedDisplay
        {
            get { return _formattedDisplay; }
            private set { SetProperty(ref _formattedDisplay, value); }
        }

        public Guid Id { get; }

        public string Name
        {
            get { return _name; }
            private set { SetProperty(ref _name, value); }
        }

        public ReadOnlyObservableCollection<IRefueling> Refuelings { get; }

        public string Registration
        {
            get { return _registration; }
            private set { SetProperty(ref _registration, value); }
        }

        [JsonIgnore]
        public IStats Stats
        {
            get { return _stats; }
            private set { SetProperty(ref _stats, value); UpdateFormattedDisplay(); }
        }

        public string UserAccountName { get; }

        #endregion

        #region IVehicle Methods

        public void AddRefueling(IRefueling refueling)
        {
            try
            {
                Log.Verbose($"{refueling}");
                if (refueling == null)
                    throw new ArgumentNullException(nameof(refueling));

                _refuelings.Insert(0, refueling);
                Stats = VehicleCalculator.CalculateVehicleStats(Refuelings);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;

            var otherVehicle = obj as Vehicle;
            if (otherVehicle == null)
                throw new ArgumentException("Object is not a vehicle");

            return string.Compare(Registration, otherVehicle.Registration, StringComparison.Ordinal);
        }

        public void DeleteRefueling(IRefueling refueling)
        {
            try
            {
                Log.Verbose($"{refueling}");
                if (refueling == null)
                    throw new ArgumentNullException(nameof(refueling));
                if (!Refuelings.Contains(refueling))
                    throw new ArgumentException(nameof(refueling));

                _refuelings.Remove(refueling);
                Stats = VehicleCalculator.CalculateVehicleStats(Refuelings);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public void Edit(string name, string registration)
        {
            try
            {
                Log.Verbose($"[{name}|{registration}]");
                if (!VehicleValidator.IsValid(name, registration))
                    throw new ArgumentException("Invalid vehicle data");
                Name = name;
                Registration = registration;
                UpdateFormattedDisplay();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public void SortRefuelings() => _refuelings.SortDescending();

        #endregion

        #region Overrides

        public override string ToString() => $"[{Id}|{Registration}|{Name}|{Stats.Consumption}|{UserAccountName}]";

        #endregion

        #region Private Fields

        private readonly ObservableCollection<IRefueling> _refuelings;

        private string _formattedDisplay;
        private string _name;
        private string _registration;
        private IStats _stats;

        #endregion

        #region Private Methods

        private void UpdateFormattedDisplay() => FormattedDisplay =
            $"{Registration} ({Name}) {Stats.Consumption.ToString("F2", new CultureInfo("de"))} l/100km";

        #endregion
    }
}
