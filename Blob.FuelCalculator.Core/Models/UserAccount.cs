﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Interfaces;
using Newtonsoft.Json;

namespace Blob.FuelCalculator.Core.Models
{
    public class UserAccount : IUserAccount
    {
        #region Constructors

        public UserAccount(string name)
            : this(name, new List<Vehicle>())
        {
        }

        [JsonConstructor]
        public UserAccount(string name, IList<Vehicle> vehicles)
        {
            try
            {
                Log.Verbose($"[{name}|{vehicles.Count}]");
                Name = name;
                Vehicles = new ObservableCollection<IVehicle>(vehicles);
            }
            catch (System.Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region IUserAccount Properties

        public string Name { get; }
        public ObservableCollection<IVehicle> Vehicles { get; }

        #endregion

        #region Overrides

        public override string ToString() => $"[{Name}]";

        #endregion
    }
}
