﻿using System;
using Blob.FuelCalculator.Core.Calculators;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Validation;
using Blob.FuelCalculator.Interfaces;
using Prism.Mvvm;

namespace Blob.FuelCalculator.Core.Models
{
    public class Stats : BindableBase, IStats
    {
        #region Constructors

        public Stats(double amount, double distance)
        {
            try
            {
                Log.Verbose($"[{amount}|{distance}]");
                if (!StatsValidator.IsValid(amount, distance))
                    throw new ArgumentException("Invalid stats data");
                Amount = amount;
                Distance = distance;
                Consumption = StatsCalculator.CalculateConsumption(amount, distance);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region IStats Properties

        public double Amount
        {
            get { return _amount; }
            private set { SetProperty(ref _amount, value); }
        }

        public double Consumption
        {
            get { return _consumption; }
            private set { SetProperty(ref _consumption, value); }
        }

        public double Distance
        {
            get { return _distance; }
            private set { SetProperty(ref _distance, value); }
        }

        #endregion

        #region Overrides

        public override string ToString() => $"[{Amount}|{Distance}|{Consumption}]";

        #endregion

        #region Private Fields

        private double _amount;
        private double _consumption;
        private double _distance;

        #endregion
    }
}
