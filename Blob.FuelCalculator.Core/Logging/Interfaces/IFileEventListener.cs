﻿using System;
using System.Diagnostics.Tracing;
using System.Threading.Tasks;

namespace Blob.FuelCalculator.Core.Logging.Interfaces
{
    public interface IFileEventListener : IDisposable
    {
        #region Methods

        void EnableEvents(EventSource eventSource, EventLevel level);
        Task Start();

        #endregion
    }
}