using System;
using System.Diagnostics.Tracing;

namespace Blob.FuelCalculator.Core.Logging.Interfaces
{
    public interface IDebugEventListener : IDisposable
    {
        #region Methods

        void EnableEvents(EventSource eventSource, EventLevel level);

        #endregion
    }
}