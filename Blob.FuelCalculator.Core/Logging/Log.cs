using System.Diagnostics.Tracing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Logging.Interfaces;

namespace Blob.FuelCalculator.Core.Logging
{
    public static class Log
    {
        #region Static Fields

        private static IDebugEventListener _debugEventListener;
        private static IFileEventListener _fileEventListener;

        #endregion

        #region Static Properties

        public static CustomEventSource EventSource { get; } = new CustomEventSource();

        #endregion

        #region Static Methods

        public static void Critical(string message, [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "") =>
                EventSource.Critical(FormatMessage(EventLevel.Critical, message, filePath, lineNumber, memberName));

        public static void Error(string message, [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "") =>
                EventSource.Error(FormatMessage(EventLevel.Error, message, filePath, lineNumber, memberName));

        public static void Info(string message, [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "") =>
                EventSource.Info(FormatMessage(EventLevel.Informational, message, filePath, lineNumber, memberName));

        public static async Task Start(IDebugEventListener debugEventListener, IFileEventListener fileEventListener)
        {
            if (debugEventListener != null)
            {
                _debugEventListener = debugEventListener;
                _debugEventListener.EnableEvents(EventSource, EventLevel.Verbose);
            }
            if (fileEventListener != null)
            {
                _fileEventListener = fileEventListener;
                await _fileEventListener.Start();
                _fileEventListener.EnableEvents(EventSource, EventLevel.Informational);
            }
        }

        public static void Stop()
        {
            _debugEventListener?.Dispose();
            _fileEventListener?.Dispose();
        }

        public static void Verbose(string message, [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "") =>
                EventSource.Log(FormatMessage(EventLevel.Verbose, message, filePath, lineNumber, memberName));

        public static void Warning(string message, [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "") =>
                EventSource.Warning(FormatMessage(EventLevel.Warning, message, filePath, lineNumber, memberName));

        private static string FormatMessage(
            EventLevel level, string message, string filePath, int lineNumber, string memberName)
        {
            var shortFilePath = filePath.Substring(filePath.IndexOf('\\') + 1);
            shortFilePath = shortFilePath.Substring(shortFilePath.IndexOf('\\') + 1);
            var className = filePath.Substring(filePath.LastIndexOf('\\') + 1).Replace(".cs", "").Replace(".xaml", "");

            var sb = new StringBuilder();
            sb.Append(className);
            sb.Append(".");
            sb.Append(memberName.TrimStart('.'));
            if (level == EventLevel.Error || level == EventLevel.Critical)
            {
                sb.Append(" (");
                sb.Append(shortFilePath);
                sb.Append(" Line ");
                sb.Append(lineNumber);
                sb.Append(")");
            }
            if (!string.IsNullOrEmpty(message))
            {
                sb.Append(": ");
                if (level == EventLevel.Error || level == EventLevel.Critical)
                    sb.Append(message.Replace("\r\n", "").Replace("   ", "").Replace("  ", ""));
                else
                    sb.Append(message);
            }
            return sb.ToString();
        }

        #endregion
    }
}