﻿using System.Diagnostics.Tracing;

namespace Blob.FuelCalculator.Core.Logging
{
    public class CustomEventSource : EventSource
    {
        #region Methods

        [Event(1, Level = EventLevel.Critical)]
        public void Critical(string message) => WriteMessage(1, message);

        [Event(2, Level = EventLevel.Error)]
        public void Error(string message) => WriteMessage(2, message);

        [Event(4, Level = EventLevel.Informational)]
        public void Info(string message) => WriteMessage(4, message);

        [Event(5, Level = EventLevel.Verbose)]
        public void Log(string message) => WriteMessage(5, message);

        [Event(3, Level = EventLevel.Warning)]
        public void Warning(string message) => WriteMessage(3, message);

        #endregion

        #region Private Methods

        private void WriteMessage(int eventId, string message) =>
            WriteEvent(eventId, message);

        #endregion
    }
}
