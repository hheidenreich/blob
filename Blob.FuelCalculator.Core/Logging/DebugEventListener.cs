using System.Diagnostics;
using System.Diagnostics.Tracing;
using Blob.FuelCalculator.Core.Logging.Interfaces;

namespace Blob.FuelCalculator.Core.Logging
{
    public class DebugEventListener : EventListener, IDebugEventListener
    {
        #region Protected Overrides

        protected override void OnEventWritten(EventWrittenEventArgs eventData) =>
            Debug.WriteLine($"{eventData.Level.ToString().ToUpper().Substring(0, 3)} {eventData.Payload[0]}");

        #endregion
    }
}