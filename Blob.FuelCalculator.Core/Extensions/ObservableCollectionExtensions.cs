﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Blob.FuelCalculator.Core.Logging;

namespace Blob.FuelCalculator.Core.Extensions
{
    public static class ObservableCollectionExtensions
    {
        #region Static Methods

        public static void Sort<T>(this ObservableCollection<T> collection) where T : IComparable
        {
            try
            {
                Log.Verbose(null);
                var sorted = collection.OrderBy(x => x).ToList();
                for (var i = 0; i < sorted.Count(); i++)
                    collection.Move(collection.IndexOf(sorted[i]), i);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public static void SortDescending<T>(this ObservableCollection<T> collection) where T : IComparable
        {
            try
            {
                Log.Verbose(null);
                var sorted = collection.OrderByDescending(x => x).ToList();
                for (var i = 0; i < sorted.Count(); i++)
                    collection.Move(collection.IndexOf(sorted[i]), i);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion
    }
}
