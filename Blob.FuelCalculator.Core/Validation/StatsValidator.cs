﻿namespace Blob.FuelCalculator.Core.Validation
{
    public static class StatsValidator
    {
        #region Static Properties

        public static double MinAmount { get; } = 0.0;
        public static double MinDistance { get; } = 0.0;

        #endregion

        #region Static Methods

        public static bool IsValid(double amount, double distance) => !(amount < MinAmount || distance < MinDistance);

        #endregion
    }
}
