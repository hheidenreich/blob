﻿using System.Text.RegularExpressions;

namespace Blob.FuelCalculator.Core.Validation
{
    public static class VehicleValidator
    {
        #region Static Methods

        public static bool IsValid(string name, string registration) =>
            !string.IsNullOrEmpty(name) && IsValidRegistration(registration);

        private static bool IsValidRegistration(string registration) =>
            registration != null && Regex.IsMatch(registration, @"^[A-Z]{1,3}[-][A-Z]{1,2} [0-9]{1,4}$");

        #endregion
    }
}
