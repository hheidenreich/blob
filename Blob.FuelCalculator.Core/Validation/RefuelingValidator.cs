﻿namespace Blob.FuelCalculator.Core.Validation
{
    public static class RefuelingValidator
    {
        #region Static Properties

        public static double MaxAmount { get; } = 200.0;
        public static double MaxDistance { get; } = 2000.0;
        public static double MinAmount { get; } = 1.0;
        public static double MinDistance { get; } = 1.0;

        #endregion

        #region Static Methods

        public static bool IsValid(double amount, double distance) =>
            !(amount < MinAmount || amount > MaxAmount || distance < MinDistance || distance > MaxDistance);

        #endregion
    }
}
