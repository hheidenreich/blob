﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;

namespace Blob.FuelCalculator.Core.Services
{
    public class HttpClientService : IHttpClientService
    {
        #region Constructors

        public HttpClientService(IWebStorageDataService webStorageDataService)
        {
            Log.Info(null);
            _webStorageDataService = webStorageDataService;
        }

        #endregion

        #region IHttpClientService Methods

        public async Task DeleteRequestAsync(string api, Guid id)
        {
            try
            {
                Log.Info($"[{api}|{id}]");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _webStorageDataService.ClientBaseAddress;

                    using (var response = await client.DeleteAsync(api + "/" + id))
                    {
                        Log.Info($"[{response.StatusCode}]");
                        if (!response.IsSuccessStatusCode)
                            throw new Exception($"Invalid response: {response.StatusCode}.");
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task<string> GetRequestAsync(string api)
        {
            try
            {
                Log.Info($"[{api}]");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _webStorageDataService.ClientBaseAddress;
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(_webStorageDataService.JsonHeader);

                    using (var response = await client.GetAsync(api))
                    {
                        Log.Info($"[{response.StatusCode}]");
                        if (response.IsSuccessStatusCode)
                            return await response.Content.ReadAsStringAsync();
                        if (response.StatusCode == HttpStatusCode.NotFound)
                            return null;
                        throw new Exception($"Invalid response: {response.StatusCode}.");
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task<string> GetRequestAsync(string api, string id) => await GetRequestAsync(api + "/" + id);

        public async Task<bool> IsAvailableAsync()
        {
            try
            {
                Log.Info(null);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _webStorageDataService.ClientBaseAddress;

                    using (var response = await client.GetAsync(_webStorageDataService.UserAccountsApi))
                        return true;
                }
            }
            catch (HttpRequestException)
            {
                return false;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task PostRequestAsync(string api, string serialized)
        {
            try
            {
                Log.Info($"[{api}|{serialized}]");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _webStorageDataService.ClientBaseAddress;

                    using (var response = await client.PostAsync(
                        api, new StringContent(serialized, Encoding.Unicode, _webStorageDataService.MediaType)))
                    {
                        Log.Info($"[{response.StatusCode}]");
                        if (!response.IsSuccessStatusCode)
                            throw new Exception($"Invalid response: {response.StatusCode}.");
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task PutRequestAsync(string api, Guid id, string serialized)
        {
            try
            {
                Log.Info($"[{api}|{serialized}]");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _webStorageDataService.ClientBaseAddress;

                    using (var response = await client.PutAsync(
                        api + "/" + id,
                        new StringContent(serialized, Encoding.Unicode, _webStorageDataService.MediaType)))
                    {
                        Log.Info($"[{response.StatusCode}]");
                        if (!response.IsSuccessStatusCode)
                            throw new Exception($"Invalid response: {response.StatusCode}.");
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region Private Fields

        private readonly IWebStorageDataService _webStorageDataService;

        #endregion
    }
}
