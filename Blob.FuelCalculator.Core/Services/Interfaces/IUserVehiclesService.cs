using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Core.Services.Interfaces
{
    public interface IUserVehiclesService
    {
        #region Properties

        ReadOnlyObservableCollection<IVehicle> Vehicles { get; }

        #endregion

        #region Methods

        Task AddRefuelingAsync(Vehicle vehicle, double amount, double distance);
        Task AddVehicleAsync(string name, string registration);
        bool CanAddVehicle(string name, string registration);
        bool CanEditVehicle(Vehicle vehicle, string newName, string newRegistration);
        Task DeleteRefuelingAsync(Vehicle vehicle, Refueling refueling);
        Task DeleteVehicleAsync(Vehicle vehicle);
        Task EditVehicleAsync(Vehicle vehicle, string newName, string newRegistration);
        bool IsValidRefueling(double amount, double distance);
        Task<bool> StartAsync(string username);

        #endregion
    }
}