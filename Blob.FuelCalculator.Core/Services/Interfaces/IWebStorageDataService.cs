﻿using System;
using System.Net.Http.Headers;

namespace Blob.FuelCalculator.Core.Services.Interfaces
{
    public interface IWebStorageDataService
    {
        #region Properties

        Uri ClientBaseAddress { get; }
        MediaTypeWithQualityHeaderValue JsonHeader { get; }
        string MediaType { get; }
        string RefuelingsApi { get; }
        string UserAccountDataTransferObjectsApi { get; }
        string UserAccountsApi { get; }
        string VehiclesApi { get; }

        #endregion
    }
}