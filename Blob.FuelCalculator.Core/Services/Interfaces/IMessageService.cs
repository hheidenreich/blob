﻿using System.Threading.Tasks;

namespace Blob.FuelCalculator.Core.Services.Interfaces
{
    public interface IMessageService
    {
        #region Methods

        Task ShowMessageAsync(string code, string message, bool shutdown = false);

        #endregion
    }
}