﻿using System;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Models;

namespace Blob.FuelCalculator.Core.Services.Interfaces
{
    public interface IWebStorageService
    {
        #region Methods

        Task AddRefuelingAsync(Refueling refueling);
        Task AddVehicleAsync(Vehicle vehicle);
        Task DeleteRefuelingAsync(Guid id);
        Task DeleteVehicleAsync(Vehicle vehicle);
        Task EditVehicleAsync(Vehicle vehicle);
        Task<UserAccount> GetUserAccountAsync(string name);
        Task<bool> IsAvailableAsync();

        #endregion
    }
}