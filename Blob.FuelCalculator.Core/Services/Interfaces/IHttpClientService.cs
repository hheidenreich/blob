﻿using System;
using System.Threading.Tasks;

namespace Blob.FuelCalculator.Core.Services.Interfaces
{
    public interface IHttpClientService
    {
        #region Methods

        Task DeleteRequestAsync(string api, Guid id);
        Task<string> GetRequestAsync(string api);
        Task<string> GetRequestAsync(string api, string id);
        Task<bool> IsAvailableAsync();
        Task PostRequestAsync(string api, string serialized);
        Task PutRequestAsync(string api, Guid id, string serialized);

        #endregion
    }
}