﻿using System.Threading.Tasks;

namespace Blob.FuelCalculator.Core.Services.Interfaces
{
    public interface IJsonService
    {
        #region Methods

        Task<T> DeserializeAsync<T>(string serialized);
        Task<string> SerializeAsync<T>(T deserialized);

        #endregion
    }
}
