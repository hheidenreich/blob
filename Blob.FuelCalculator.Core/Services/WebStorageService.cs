﻿using System;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services.Interfaces;

namespace Blob.FuelCalculator.Core.Services
{
    public class WebStorageService : IWebStorageService
    {
        #region Constructors

        public WebStorageService(IHttpClientService httpClientService,
                                 IJsonService jsonService,
                                 IWebStorageDataService webStorageDataService)
        {
            Log.Info(null);
            _httpClientService = httpClientService;
            _jsonService = jsonService;
            _webStorageDataService = webStorageDataService;
        }

        #endregion

        #region IWebStorageService Methods

        public async Task AddRefuelingAsync(Refueling refueling)
        {
            try
            {
                Log.Info($"{refueling}");
                await _httpClientService.PostRequestAsync(
                    _webStorageDataService.RefuelingsApi, await _jsonService.SerializeAsync(refueling));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task AddVehicleAsync(Vehicle vehicle)
        {
            try
            {
                Log.Info($"{vehicle}");
                await _httpClientService.PostRequestAsync(
                    _webStorageDataService.VehiclesApi, await _jsonService.SerializeAsync(vehicle));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task DeleteRefuelingAsync(Guid id)
        {
            try
            {
                Log.Info($"[{id}]");
                await _httpClientService.DeleteRequestAsync(_webStorageDataService.RefuelingsApi, id);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task DeleteVehicleAsync(Vehicle vehicle)
        {
            try
            {
                Log.Info($"{vehicle}");
                foreach (var refueling in vehicle.Refuelings)
                    await DeleteRefuelingAsync(refueling.Id);
                await _httpClientService.DeleteRequestAsync(_webStorageDataService.VehiclesApi, vehicle.Id);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task EditVehicleAsync(Vehicle vehicle)
        {
            try
            {
                Log.Info($"{vehicle}");
                await _httpClientService.PutRequestAsync(
                    _webStorageDataService.VehiclesApi, vehicle.Id, await _jsonService.SerializeAsync(vehicle));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task<UserAccount> GetUserAccountAsync(string name)
        {
            try
            {
                Log.Info($"[{name}]");
                await AddUserAccountAsync(name);
                var content = await _httpClientService.GetRequestAsync(
                    _webStorageDataService.UserAccountDataTransferObjectsApi, name);
                Log.Verbose($"[{content}]");
                if (string.IsNullOrEmpty(content))
                    return null;
                return await _jsonService.DeserializeAsync<UserAccount>(content);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task<bool> IsAvailableAsync()
        {
            try
            {
                Log.Verbose(null);
                return await _httpClientService.IsAvailableAsync();
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region Private Fields

        private readonly IHttpClientService _httpClientService;
        private readonly IJsonService _jsonService;
        private readonly IWebStorageDataService _webStorageDataService;

        #endregion

        #region Private Methods

        private async Task AddUserAccountAsync(string name)
        {
            try
            {
                Log.Info($"[{name}]");
                var content = await _httpClientService.GetRequestAsync(_webStorageDataService.UserAccountsApi, name);
                Log.Verbose($"[{content}]");
                if (content == null)
                {
                    var userAccount = new UserAccount(name);
                    await _httpClientService.PostRequestAsync(
                        _webStorageDataService.UserAccountsApi, await _jsonService.SerializeAsync(userAccount));
                }
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion
    }
}
