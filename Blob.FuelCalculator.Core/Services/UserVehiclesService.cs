﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Extensions;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Models;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Blob.FuelCalculator.Core.Validation;
using Blob.FuelCalculator.Interfaces;

namespace Blob.FuelCalculator.Core.Services
{
    public class UserVehiclesService : IUserVehiclesService
    {
        #region Constructors

        public UserVehiclesService(IWebStorageService webStorageService)
        {
            try
            {
                Log.Info(null);
                _webStorageService = webStorageService;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region IUserVehiclesService Properties

        public ReadOnlyObservableCollection<IVehicle> Vehicles { get; private set; }

        #endregion

        #region IUserVehiclesService Methods

        public async Task AddRefuelingAsync(Vehicle vehicle, double amount, double distance)
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"{vehicle} [{amount}|{distance}]");
                if (!IsValidRefueling(amount, distance))
                    throw new ArgumentException("Invalid refueling data.");
                var refueling = new Refueling(amount, distance, vehicle.Id);
                vehicle.AddRefueling(refueling);
                vehicle.SortRefuelings();
                await _webStorageService.AddRefuelingAsync(refueling);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task AddVehicleAsync(string name, string registration)
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"[{name}|{registration}]");
                if (!CanAddVehicle(name, registration))
                    throw new ArgumentException("Invalid vehicle data.");
                var vehicle = new Vehicle(name, registration, _userAccount.Name);
                _userAccount.Vehicles.Add(vehicle);
                _userAccount.Vehicles.Sort();
                await _webStorageService.AddVehicleAsync(vehicle);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public bool CanAddVehicle(string name, string registration) =>
            VehicleValidator.IsValid(name, registration) && Vehicles.All(x => x.Registration != registration);

        public bool CanEditVehicle(Vehicle vehicle, string newName, string newRegistration)
        {
            if (vehicle == null)
                return false;
            if (vehicle.Name == newName && vehicle.Registration == newRegistration)
                return false;
            if (!VehicleValidator.IsValid(newName, newRegistration))
                return false;
            return !Vehicles.Any(x => x.Registration != vehicle.Registration && x.Registration == newRegistration);
        }

        public async Task DeleteRefuelingAsync(Vehicle vehicle, Refueling refueling)
        {
            try
            {
                Log.Verbose($"{vehicle} {refueling}");
                if (vehicle == null || refueling == null || !vehicle.Refuelings.Contains(refueling))
                    throw new ArgumentException("Invalid refueling data.");
                vehicle.DeleteRefueling(refueling);
                await _webStorageService.DeleteRefuelingAsync(refueling.Id);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task DeleteVehicleAsync(Vehicle vehicle)
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"{vehicle}");
                if (vehicle == null || !Vehicles.Contains(vehicle))
                    throw new ArgumentException("Invalid vehicle data.");
                _userAccount.Vehicles.Remove(vehicle);
                await _webStorageService.DeleteVehicleAsync(vehicle);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task EditVehicleAsync(Vehicle vehicle, string newName, string newRegistration)
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"{vehicle} [{newName}|{newRegistration}]");
                if (!CanEditVehicle(vehicle, newName, newRegistration))
                    throw new ArgumentException("Invalid vehicle data.");
                vehicle.Edit(newName, newRegistration);
                await _webStorageService.EditVehicleAsync(vehicle);
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public bool IsValidRefueling(double amount, double distance) => RefuelingValidator.IsValid(amount, distance);

        public async Task<bool> StartAsync(string username)
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"[{username}]");
                if (!await _webStorageService.IsAvailableAsync())
                    return false;
                _userAccount = await _webStorageService.GetUserAccountAsync(username);
                _userAccount.Vehicles.Sort();
                foreach (var vehicle in _userAccount.Vehicles)
                    vehicle.SortRefuelings();
                Vehicles = new ReadOnlyObservableCollection<IVehicle>(_userAccount.Vehicles);
                return true;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion

        #region Private Fields

        private readonly IWebStorageService _webStorageService;

        private UserAccount _userAccount;

        #endregion
    }
}
