using System;
using System.Threading.Tasks;
using Blob.FuelCalculator.Core.Logging;
using Blob.FuelCalculator.Core.Services.Interfaces;
using Newtonsoft.Json;

namespace Blob.FuelCalculator.Core.Services
{
    public class JsonService : IJsonService
    {
        #region Constructors

        public JsonService()
        {
            Log.Info(null);
        }

        #endregion

        #region IJsonService Methods

        public async Task<T> DeserializeAsync<T>(string serialized)
        {
            try
            {
                Log.Info(null);
                Log.Verbose($"[{serialized}]");
                return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<T>(serialized));
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        public async Task<string> SerializeAsync<T>(T deserialized)
        {
            try
            {
                Log.Info(null);
                var serialized = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(deserialized));
                Log.Verbose($"[{serialized}]");
                return serialized;
            }
            catch (Exception exception)
            {
                Log.Error($"[{exception.Message}]");
                throw;
            }
        }

        #endregion
    }
}